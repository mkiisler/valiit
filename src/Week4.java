import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Week4 {

    public static void main(String[] args) {

        System.out.println(concatenateStrings("Hinne: ", (byte) 3));

        System.out.println(); // vaherida

//        int[] a1 = {1, 1, 1};
//        int[] a2 = {1, 1, 0};
//
//        System.out.println(arvutaMassiivideSumma(a1, a2,6));

        // sama lühemalt
        System.out.println(arvutaMassiivideSumma(new int[]{1, 2, 3}, new int[]{1, 2, 3}, 6));


        System.out.println(); //vaherida


        int[] x = ytleSonadePikkus(new String[]{"auto", "ratas"});
        for (int a : x) {
            System.out.println(a);
        }
        System.out.println(x);

        System.out.println(); //vaherida

        boolean[] u = makeBoolean(7);
        for (boolean c : u) {
            System.out.println(c);

        }

        System.out.println(); // vaherida

        String[] mm = makeHashMarksArray(7);
        for (String cc : mm) {
            System.out.println(cc);

        }

        System.out.println(); //vaherida

        // meetodis tehtud listi väljaprintimine

        System.out.println(textUnifier("punane", "kollane", "roheline"));

        System.out.println(); //vaherida

        // all meetodiga tehtud massiivi väljaprintimine
        String[] massiiv = textUnifier2("sinine", "oranz", "lilla");
        for (int ms = 0; ms < 3; ms++) {
            System.out.println(massiiv[ms]);
        }

        System.out.println(); //vaherida

        String[] massiiv2 = textUnifier2("helepunane", "punane", "tumepunane");
        for (String mmm : massiiv2) {
            System.out.println(mmm);
        }

        System.out.println(); //vaherida


    }

    public static String concatenateStrings(String s, byte b) {
        return s + b;
    }

    public static int arvutaMassiivideSumma(int[] arvuMassiiv1, int[] arvuMassiiv2, int arvLahutamiseks) {
        int massiivideSumma = 0;
        for (int i = 0; i < arvuMassiiv1.length; i++) {
            massiivideSumma += arvuMassiiv1[i];

        }
        for (int i = 0; i < arvuMassiiv2.length; i++) {
            massiivideSumma += arvuMassiiv2[i];

        }

        return massiivideSumma - arvLahutamiseks;


    }

    /* Tekita meetod, mis võtab sisendiks stringide massiivi ja mis tagastab täisarvuce massiivi.
           Iga tagastatava massiivi element on vastava sisendmassiivi elemendi pikkus.
           Näide:
           sisend: {"auto", "ratas"}
           väljund: {4, 5}
           */

    public static int[] ytleSonadePikkus(String[] sisendMassiiv) {
        int[] sonadePikkused = new int[sisendMassiiv.length];

        for (int i = 0; i < sisendMassiiv.length; i++) {
            sonadePikkused[i] = sisendMassiiv[i].length();
        }


        return sonadePikkused;
    }


    // Õpetaja lahendus - tegelikult sama

    public static int[] x4(String[] aaa) { //sisendparameetril peab olema alati nimi
        int[] bbb = new int[aaa.length]; // massiivi defineerimisel peab olema pikkus, ilma ei saa

        for (int i = 0; i < aaa.length; i++) {
            bbb[i] = aaa[i].length();
        }


        return bbb;
    }

    public static boolean[] makeBoolean(int x) {
        boolean[] xxx = new boolean[x];
        for (int j = 0; j < x; j++) {
            xxx[j] = true;
        }
        return xxx;
    }

    public static String[] makeHashMarksArray(int arv) {
        String[] zzz = new String[arv];
        for (int n = 0; n < arv; n++) {
            zzz[n] = "";
            for (int m = 0; m <= n; m++) {
                zzz[n] += "#";
            }

        }
        return zzz;
    }

    // funktsioon, mis tagastab listi stringidest

    public static List<String> textUnifier(String a, String b, String c) {

//        List<String> stringList = new ArrayList<>();
//
//        stringList.add(a);
//        stringList.add(b);
//        stringList.add(c);

        List<String> stringList = Arrays.asList(a, b, c);

        return stringList;
    }

    // funktsioon, mis tagastab stringide massiivi

    public static String[] textUnifier2(String a, String b, String c) {
        String[] massiiv = new String[3];

        massiiv[0] = a;
        massiiv[1] = b;
        massiiv[2] = c;


        return massiiv;
    }


}




