public class Day3While {

    public static void main(String[] args) {

        boolean notEnoughtMoney = true;
        int sum = 0;

        while (notEnoughtMoney) {

            sum = sum + 50; // kutsume välja mingi funktsiooni, mis tagastab meile lisanduva rahahulga ???
            if (sum >= 5000) {
                notEnoughtMoney = false;
            }

        }
        System.out.println("Teenitav raha: " + sum);
// while-tsükkel kestab nii kaua, kuni tõeväärtus muutub valeks

        // do-while tsükkel teeb kõigepealt tsükli läbi ja siis hakkab while'i tingimust kontrollima

    }
}
