import java.util.Scanner;

public class Day3 {

    // Konstandid suurte tähtedega (SCREAMING_SNAKE_CASE). "final" - ei saa muuta

    public static final double VAT_RATE = 0.2;

    public static void main(String[] args) {

        String myText = "Tallinna, Tartu, Valga, Rapla";
        Scanner scanner = new Scanner(myText);
        scanner.useDelimiter(", ");

        while (scanner.hasNext()) {
            System.out.println(scanner.next());

        }

        // StringBuilder

        StringBuilder myStringBuilder = new StringBuilder();
        myStringBuilder.append("Marek, "); // append meetod lisab tekstid üksteise otsa
        myStringBuilder.append("Rein, ");
        myStringBuilder.append("Liina, ");
        String allTheNames = myStringBuilder.toString();
        System.out.println(allTheNames);



//    public static void main(String[] args) {
//
//        String text1 = "Tere";
//        String text2 = "3";
//
//        int numl = Integer.parseInt(text2); // tekst numbriks !!!
////        String text3 = num1.toString(); // number tekstiks?
//        String text3 = String.valueOf(num1); // number tekstiks !!!
//
//        int myBigNumber = 777;
//        byte mySmallNumber = (byte)myBigNumber;
//        System.out.println(mySmallNumber);


    }

}
