import java.sql.SQLOutput;

public class Day2 {

    public static void main(String[] args) {

        byte b1 = 65; // väike number
        char c1 = 'B'; // üks tähemärk
        short s1 = 4578; // keskmise suurusega number
        int i1 = 56_569_000; // suured numbrid
        long l1 = 10_000_000_000_000L; // väga suured numbrid
        boolean istItsSpring = false; // tõeväärtus
        float f1 = 45.678f; // komakohaga arv, natuke ebatäpne
        double d1 = 567.98765; // suurem komakohaga arv, natuke ebatäpne

        long l3 = 10_000_000_000_000L;
        int i3 = (int) l3;

        /*System.out.println((char)b1);
        System.out.println(i3);*/

        // mooduliga (jäägiga)jagamine

        int i14 = 11;
        int i15 = 10;
        /*System.out.println(i14 % 10);
        System.out.println(i15 % 10);*/

        Integer i20 = 20;
//        Integer i21 = new Integer(value: 20); // Miks punane?

        /*Loo kolm arvulist muutujat a = 1, b = 1, c = 3
        Prindi välja a == b ja a == c
        Lisa koodi rida a = c
        Prindi välja a == b ja a == c, mis muutus ja miks?*/


        int a = 1;
        int b = 1;
        int c = 3;

//        System.out.println(a==b);
//        System.out.println(a==c);

        a = c;

//        System.out.println(a==b);
//        System.out.println(a==c);

       /* Loo muutujad x1 = 10 ja x2 = 20, vali sobiv andmetüüp
        Tekita muutuja y1 = ++x1, trüki välja nii x1 kui y1
        Tekita muutuja y2 = x2++, trüki välja nii x2 ja y2
        Analüüsi tulemusi*/

        int x1 = 10;
        int x2 = 20;
        int y1 = ++x1;
        int y2 = x2++;

//        System.out.println(x1);
//        System.out.println(y1);
//
//        System.out.println(x2);
//        System.out.println(y2);

       /* Loo arvulised muutujad
        a = 18 % 3
        b = 19 % 3
        c = 20 % 3
        d = 21 % 3
        Prindi välja kõigi muutujate väärtused*/

//        System.out.println("Isa ütles: \"Tule siia!\"");
//        System.out.println("Faili asukoht: C:\\test\\test.txt");
//        System.out.println("Esimene rida.\nTeine rida.");


        String myText1 = "See on tekst. ";
        String myText2 = "See on teine tekst.";
        myText1 = myText1 + myText2;

//        System.out.println(myText1);
//
//
//        System.out.println(myText1.equals(myText2)); // korrektne viis sõnede võrdelmiseks
//        System.out.println(myText1.compareToIgnoreCase(anotherString:"t3");

        /*Hello, World!
● Hello “World”!
● Steven Hawking once said: “Life would be tragic if it weren’t
        funny”.
● Kui liita kokku sõned “See on teksti esimene pool ” ning “See
        on teksti teine pool”, siis tulemuseks saame “See on teksti
        esimene pool See on teksti teine pool”.
● Elu on ilus.
● Elu on ‘ilus’.
● Elu on “ilus”.
● Kõige rohkem segadust tekitab “-märgi kasutamine sõne sees.
● Eesti keele kõige ilusam lause on: “Sõida tasa üle silla!”
● ‘Kolm’ - kolm, ‘neli’ - neli, “viis” - viis.*/

        System.out.println("Hello, World!");
        System.out.println("Hello \"World\"!");
        System.out.println("Steven Hawking once said: \"Life would be tragic if it weren’t funny\".\n");

        System.out.println("Kui liita kokku sõned \"See on teksti esimene pool \" ning \"See \n" +
                "on teksti teine pool\", siis tulemuseks saame \"See on teksti \nesimene pool See on teksti teine pool\".\n");

        System.out.println("Elu on ilus.");
        System.out.println("Elu on 'ilus'.");
        System.out.println("Elu on \"ilus\".\n");

        System.out.println("Kõige rohkem segadust tekitab \"-märgi kasutamine sõne sees.");
        System.out.println("Eesti keele kõige ilusam lause on: \"Sõida tasa üle silla!\"");
        System.out.println("'Kolm' - kolm, 'neli' - neli, \"viis\" - viis.\n\n");


        /*Ülesanne 1:
1. Defineeri String-tüüpi muutuja tallinnPopulation​. ​Anna
muutujale väärtus “450 000”.
2. Kirjuta standardväljundisse järgmine lause: “Tallinnas elab
450 000 inimest”​, kus rahvaarvu number pärineb muutujast
tallinnPopulation​.
3. Defineeri muutuja populationOfTallinn​ täisarvuna. Prindi
standardväljundisse sama lause, mis punktis 2, kasutades
muutujat populationOfTallinn​.*/

        String tallinnPopulation​ = "450 000";
        System.out.println("Tallinnas elab " + tallinnPopulation​ + " inimest.");

        int populationOfTallinn = 450_000;
        System.out.println("Tallinnas elab " + (int)populationOfTallinn + " inimest.");

        int populationOfTallinn2 = 450_000;
        System.out.println(String.format("Tallinnas elab %,d inimest.\n", populationOfTallinn2));




/*Ülesanne 2:
● Defineeri muutuja bookTitle​.
● Omista muutujale väärtus “Rehepapp”
● Prindi standardväljundisse tekst: “Raamatu “Rehepapp” autor
on Andrus Kivirähk”​, kus raamatu nimi pärineb muutujast
bookTitle​.*/

        String bookTitle = "Rehepapp";
        System.out.println("Raamatu " + bookTitle + " autor on Andrus Kivirähk.\n");



        /*Ülesanne 3:
● Defineeri muutujad
○ planet1 ​väärtusega “Merkuur”
○ planet2 ​väärtusega “Venus”
○ planet3 ​väärtusega “Maa”
○ planet4 ​väärtusega “Marss”
○ planet5 ​väärtusega “Jupiter”
○ planet6 ​väärtusega “Saturn”
○ planet7 ​väärtusega “Uran”
○ planet8 ​väärtusega “Neptuun”
○ planetCount​ väärtusega 8
● Prindi standardväljundisse lause: “Merkuur, Veenus, Maa,
                Marss, Jupiter, Saturn, Uraan ja Neptuun on Päikesesüsteemi 8
        planeeti”​, kasutades eelnevalt defineeritud muutujaid.
● Prindi standardväljundisse sama lause, aga kasuta seejuures
        abifunktsiooni String.format()​.*/


        String planet1 = "Merkuur";
        String planet2 = "Veenus";
        String planet3 = "Maa";
        String planet4 = "Marss";
        String planet5 = "Jupiter";
        String planet6 = "Saturn";
        String planet7 = "Uran";
        String planet8 = "Neptuun";
        int planetCount = 8;

        System.out.println(
                planet1 + ", " + planet2 + ", " + planet3 + ", " + planet4 + ", " + planet5 + ", " + planet6 + ", " + planet7 + ", " + planet8 +
                        " on Päikesesüsteemi " + planetCount + " planeeti."
        );

        System.out.println(String.format(
                "%s, %s, %s, %s, %s, %s, %s, %s on Päikesesüsteemi %d planeeti.",
                planet1, planet2, planet3, planet4, planet5, planet6, planet7, planet8, planetCount
                )
        );


    }

}
