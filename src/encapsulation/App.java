package encapsulation;

public class App {
    public static void main(String[] args) {

        Human human1 = new Human("Marek", 38, 76);
        System.out.println("Inimese nimi on " + human1.getName());
        human1 = new Human("Peeter", human1.getAge(), human1.getWeight()); // ei muuda Mareki onjekti, vaid asendab selle, sest andmekandja on muutumatu
        System.out.println("Inimese nimi on " + human1.getName());
    }
}
