import java.util.*;

public class Day4Kollektsioonid {

    public static void main(String[] args) {

        List<String> names = new ArrayList<>();
        names.add("Thomas");
        names.add("Mary");
        names.add("Linda");
        System.out.println(names);

        // Mis kohal asub Linda?
        System.out.println(names.indexOf("Linda"));

        // Lisa kohale 1 nimi Ruth
        names.add(1, "Ruth");
        System.out.println(names);

        // Kas Linda on listis?
        System.out.println(names.contains("Linda"));

        // Asendab "Linda" "Lauraga", kui tead indeksit
        names.set(2, "Laura"); // praegu asendab vale nime, sest olen hiljem lisanud "Ruthi"

        // Leiab Linda indeksi ja asendab "Linda" "Lauraga"
        int lindaPosition = names.indexOf("Linda");
        if (lindaPosition >= 0) {
            names.set(lindaPosition, "Laura");
        }

        System.out.println(names);

        // Nimekirja väljaprintimine
        for (String name : names) {
            System.out.println("Nimi: " + name);
        }

        // Siia komplekslisti näited

//        List<List<String>> complexList = new
//                Arrays.asList("Mark", "Thomas", "Linda");


        // SET - indeksit ei ole, hulga elemendid on segamini ja unikaalsed

        Set<String> uniqueNames = new HashSet<>();
        uniqueNames.add("Mary");
        uniqueNames.add("Thomas");
        uniqueNames.add("Mark");
        uniqueNames.add("Mary");

        System.out.println(uniqueNames);

        for (String uniqueName : uniqueNames) {
            System.out.println(("Unikaalne nimi: " + uniqueName));

        }

        // Eemaldab Marki hulgast
        uniqueNames.remove("Mark");
        System.out.println(uniqueNames);

        // TreeSet - sorteerimine tähestiku järjekorras

        Set<String> sortedNames = new TreeSet<>();
        sortedNames.add("Mary");
        sortedNames.add("Thomas");
        sortedNames.add("Mark");
        sortedNames.add("Mary");
        System.out.println(sortedNames);

        //Objektide võrdlemine - poolik
//
//        Comparator<String> name ...


        // MAP

        Map<String, String> phoneNumbers = new HashMap<>();
        phoneNumbers.put("Mati", "0372 55 262 262");
        phoneNumbers.put("Kati", "0372 55 262 263");
        phoneNumbers.put("Mari", "0372 55 262 264");

        System.out.println(phoneNumbers.get("Mati"));
        System.out.println(phoneNumbers.keySet());

        for (String key : phoneNumbers.keySet()) {
            System.out.println("Sõber: " + key + ", telefon " + phoneNumbers.get(key));
        }

        // tel nr asendamine
        phoneNumbers.put("Mary", "50 605 403 030");
        // tel nr lisamine
        phoneNumbers.put("Leonardo", "0452 678 292");
        for (String key : phoneNumbers.keySet()) {
            System.out.println("Sõber: " + key + ", telefon " + phoneNumbers.get(key)); // Miks prindib esimesed nimed kaks korda ???
        }

        // TreeMap - vt näidet õpetaja failis
        // Mitmetasandiline MAP - vt õpetaja faili

    }

}

