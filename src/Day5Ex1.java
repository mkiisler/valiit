import java.util.*;

public class Day5Ex1 {

    public static void main(String[] args) {


/*
Ülesanne 1

Kirjuta kood, mis tekitaks konsooli järgneva pildi (kasuta tsükleid ja if-lauseid):

######
#####
####
###
##
#   */


        int sideLength = 6;
        for (int row = 0; row < sideLength; row++) {
            int columnCount = sideLength - row;

            for (int column = 0; column < columnCount; column++) {
                System.out.print("#");

            }
            System.out.println("");
        }

        System.out.println("");
       /* Ülesanne 2

Kirjuta kood, mis tekitaks konsooli järgneva pildi (kasuta tsükleid ja if-lauseid):

#
##
###
####
#####
######  */

        int sideLength2 = 6;
        for (int row2 = 0; row2 < sideLength2; row2++) {
            for (int column2 = 0; column2 <= row2; column2++) {
                System.out.print("#");

            }
            System.out.println("");
        }
        System.out.println("");

        /*Ülesanne 3

Kirjuta kood mis tekitaks konsooli järgneva pildi (kasuta tsükleid ja if-lauseid):

    @
   @@
  @@@
 @@@@
@@@@@*/

        int sideLength3 = 5;
        for (int row3 = 0; row3 < sideLength2; row3++) {
            int columnCount3 = sideLength3 - row3;
            for (int row32 = 0; row32 <= sideLength3 - row3; row3++) {
                System.out.print(" ");
                for (int column3 = 0; column3 < row3; column3++) {
                    System.out.print("@");
                }


                System.out.println("");
            }
            System.out.println(""); // et ülesannete vahele jääks rida
        }

// 2. variant - õpetaja oma

        int sideLength4 = 5;
        for (int row4 = 0; row4 < sideLength4; row4++) {
            for (int column4 = 0; column4 < sideLength4; column4++) {
                if (column4 < sideLength4 - row4 - 1) {
                    System.out.print(" ");
                } else {
                    System.out.print("@");

                }
            }
            System.out.println("");

        }

        /*Ülesanne 4

Numbri pööramine: pööra ringi etteantud number, näiteks
1234567 -> 7654321
389 -> 983 */

        int number = 1234567;
        String numberText = String.valueOf(number);
        char[] numberChars = numberText.toCharArray();
        String reversedNumberText = "";
        /*for (char c : numberChars){
            reversedNumberText = c + reversedNumberText*/

        for (int i = 0; i < numberChars.length; i++) {
            reversedNumberText = reversedNumberText + numberChars[numberChars.length - i - 1];
        }
        int reversedNumber = Integer.parseInt(reversedNumberText);

        System.out.println(reversedNumber);


        // Variant 2 - poolik, vaata õpetaja failist

        System.out.println();
        int number2 = 1234567;
        String numberText2 = String.valueOf(number2);
        StringBuilder numberReversedBuilder = new StringBuilder(numberText2);
        String reversedNumberText2 = numberReversedBuilder.reverse().toString();
        int reversedNumber2 = Integer.parseInt(reversedNumberText2);
        System.out.println(reversedNumber2);


        //
        // Variant 3 - vt õpetaja failist

//        System.out.println();


       /* Ülesanne 5

        Kirjuta main-meetod, mis loeb käsurealt õpilase nime ja tema eksami punktide summa. Kui summa on
        väiksem kui 51 punkti, väljastatakse sõna “FAIL”, muudel juhtudel väljastatakse “[NIMI]:PASS - hinne, punkti summa”,
        kus hinne on arvutatud järgmiselt:
        1 = 51-60
        2 = 61-70
        3 = 71-80
        4 = 81-90
        5 = 91-100*/


//        int punktiSumma;
//        String nimi;
//
//        switch () {
//            case (punktiSumma < 51):
//                System.out.println("[" + nimi + "]: FAIL");
//            case (punktiSumma >= 51 && punktiSumma <= 60):
//                System.out.println("[" + nimi + "]: PASS - 1, " + punktiSumma);


        // Õpetaja lahendus

        //       String studentInfo = "Marek, 99";
//       String[] studentInfoArray = studentInfo.split("");


        if (args.length >= 2) { //Kas on üldse parameetreid, mida analüüsida
            String studentName = args[0];
            int studentScore = Integer.parseInt(args[1]);
            String resultingText = studentName + ": ";

            if (studentScore > 90) {
                resultingText = resultingText + "PASS - 5, " + studentScore;
            } else if (studentScore > 80) {
                resultingText = resultingText + "PASS - 4, " + studentScore;
            } else if (studentScore > 70) {
                resultingText = resultingText + "PASS - 3, " + studentScore;
            } else if (studentScore > 60) {
                resultingText = resultingText + "PASS - 2, " + studentScore;
            } else if (studentScore > 50) {
                resultingText = resultingText + "PASS - 5, " + studentScore;
            } else {
                resultingText = "FAIL";
            }

            System.out.println(resultingText);


        }

        System.out.println();

      /*  Ülesanne 6

        Loo komaga arvudega massiiv (kahetasandiline massiiv), kus oleks kaks arvu igas reas. Massiivi läbides trüki
        välja nende arvude ruutude summa ruutjuur. VASTUS =a2+b2.

                Vaja on kasutada matemaatilisi funktsioone Math.pow() ja Math.sqrt().*/

        double[][] triangleSideList = {
                {1.5, 2},
                {3.6, 4},
                {5.7, 6},
                {7.7, 8},
                {9.8, 1}
        };

        for (double[] triangleSides : triangleSideList) {
            double c = Math.sqrt(Math.pow(triangleSides[0], 2) + Math.pow(triangleSides[1], 2));
            System.out.println("Kolmnurga hüpotenuusi pikkus: " + c);

        }
        System.out.println();

       /* Ülesanne 7

        Tekita kahetasandiline massiiv, mis hoiaks infot riigi, selle pealinna, ja peaministri nimega (massiivi minimaalsed mõõtmed: 10X3) Näiteks:
        Estonia, Tallinn, Jüri Ratas
        Latvia, Riga, Arturs Krišjānis Kariņš
...


        Trüki välja ainult riikide peaministrid - iga nimi eraldi real.
                Trüki välja iga riigi kohta rida: Country: XXX, Capital: YYY, Prime minister: ZZZ*/

        String[][] riikPealinnPeaminister = new String[10][3];
        riikPealinnPeaminister[0] = new String[]{"Eesti", "Tallinn", "Jüri Ratas"};
        riikPealinnPeaminister[1] = new String[]{"Läti", "Riia", "Läti Peaminister"};
        riikPealinnPeaminister[2] = new String[]{"Leedu", "Vilnius", "Leedu Peaminister"};
        riikPealinnPeaminister[3] = new String[]{"Saksamaa", "Berliin", "Saksamaa Peaminister"};
        riikPealinnPeaminister[4] = new String[]{"Itaalia", "Rooma", "Itaalia Peaminister"};
        riikPealinnPeaminister[5] = new String[]{"Prantsusmaa", "Pariis", "Prantuse Peaminister"};
        riikPealinnPeaminister[6] = new String[]{"Suurbritannia", "London", "Suurbritannia Peaminister"};
        riikPealinnPeaminister[7] = new String[]{"Tšehhi", "Praha", "Tšehhi Peaminister"};
        riikPealinnPeaminister[8] = new String[]{"Norra", "Oslo", "Norra Peaminister"};
        riikPealinnPeaminister[9] = new String[]{"Rootsi", "Stockholm", "Rootsi Peaminister"};

        String[][] riikPealinnPeaminister2 = {{"Eesti", "Tallinn", "Jüri Ratas"},
                {"Läti", "Riia", "Läti Peaminister"},
                {"Leedu", "Vilnius", "Leedu Peaminister"},
                {"Saksamaa", "Berliin", "Saksamaa Peaminister"},
                {"Itaalia", "Rooma", "Itaalia Peaminister"},
                {"Prantsusmaa", "Pariis", "Prantuse Peaminister"},
                {"Suurbritannia", "London", "Suurbritannia Peaminister"},
                {"Tšehhi", "Praha", "Tšehhi Peaminister"},
                {"Norra", "Oslo", "Norra Peaminister"},
                {"Rootsi", "Stockholm", "Rootsi Peaminister"}};

        for (String[] country : riikPealinnPeaminister) {
            System.out.println(country[2]);
        }

        for (int i = 0; i < riikPealinnPeaminister2.length; i++) {
            System.out.println(String.format("Country: %s, Capital: %s, Prime minister: %s",
                    riikPealinnPeaminister2[i][0], riikPealinnPeaminister2[i][1], riikPealinnPeaminister2[i][2]));
        }



        /*Ülesanne 8

        Tekita eelmise ülesandega sarnane massiiv, aga lisa igale riigile veel ka selles riigis räägitavad levinumad keeled. Näiteks:
        Estonia, Tallinn, Jüri Ratas, [Estonian, Russian, Ukrainian, Belarusian, Finnish]
        Latvia, Riga, Māris Kučinskis, [Latvian, Russian, Belarusian, Ukrainian, Polish]
...

        Prindi välja riigi nimi ja selle alla kõik räägitavad keeled. Umbes nii:
        Estonia:
        Estonian Russian Ukrainian Belarusian Finnish*/

        Object[][] riikJaKeeled = {
                {"Eesti", "Tallinn", "Jüri Ratas", new String[]{"eesti", "vene"}},
                {"Läti", "Riia", "Läti Peaminister", new String[]{"läti", "vene"}},
                {"Leedu", "Vilnius", "Leedu Peaminister", new String[]{"leedu", "vene"}},
                {"Saksamaa", "Berliin", "Saksamaa Peaminister", new String[]{"saksa", "türgi"}},
                {"Itaalia", "Rooma", "Itaalia Peaminister", new String[]{"itaalia", "saksa"}},
                {"Prantsusmaa", "Pariis", "Prantuse Peaminister", new String[]{"prantsuse", "vene"}},
                {"Suurbritannia", "London", "Suurbritannia Peaminister", new String[]{"inglise", "iiri", "šoti"}},
                {"Tšehhi", "Praha", "Tšehhi Peaminister", new String[]{"tšehhi", "slovaki"}},
                {"Norra", "Oslo", "Norra Peaminister", new String[]{"norra", "saami"}},
                {"Rootsi", "Stockholm", "Rootsi Peaminister", new String[]{"rootsi", "saami"}}};

        for (Object[] country2 : riikJaKeeled) {
            System.out.println(country2[0] + ":");
            for (String language : (String[]) country2[3]) {
                System.out.print(language + " ");
            }
            System.out.println();
        }

        System.out.println();


       /* Ülesanne 9

        Korda eelnevat ülesannet, aga kasuta andmete hoidmiseks List-tüüpi objekti.*/

        List<List<Object>> riikJaKeeled2 = Arrays.asList(
                Arrays.asList("Eesti", "Tallinn", "Jüri Ratas", Arrays.asList("eesti", "vene")),
                Arrays.asList("Läti", "Riia", "Läti Peaminister", Arrays.asList("läti", "vene")),
                Arrays.asList("Leedu", "Vilnius", "Leedu Peaminister", Arrays.asList("leedu", "vene")),
                Arrays.asList("Saksamaa", "Berliin", "Saksamaa Peaminister", Arrays.asList("saksa", "türgi")),
                Arrays.asList("Itaalia", "Rooma", "Itaalia Peaminister", Arrays.asList("itaalia", "saksa")),
                Arrays.asList("Prantsusmaa", "Pariis", "Prantuse Peaminister", Arrays.asList("prantsuse", "vene")),
                Arrays.asList("Suurbritannia", "London", "Suurbritannia Peaminister", Arrays.asList("inglise", "iiri", "šoti")),
                Arrays.asList("Tšehhi", "Praha", "Tšehhi Peaminister", Arrays.asList("tšehhi", "slovaki")),
                Arrays.asList("Norra", "Oslo", "Norra Peaminister", Arrays.asList("norra", "saami")),
                Arrays.asList("Rootsi", "Stockholm", "Rootsi Peaminister", Arrays.asList("rootsi", "saami"))
        );

        for (List<Object> country3 : riikJaKeeled2) {
            System.out.println(country3.get(0) + ":");
            for (String language : (List<String>) country3.get(3)) {
                System.out.print(language + " ");
            }

            System.out.println();

        /*Ülesanne 10

        Korda eelnevat ülesannet, aga kasuta andmete hoidmiseks Map-tüüpi objekti, kus key-väärtus oleks riigi nimi.*/

            // Järjestamise kooda, aga poolik:
            // Comparator<String> countryNameLengthComparator = (countryName1, countryName2) -> {countryName1.length() - countryName2.length()
            //  && countryName1.compareTo(countryName2

            Map<String, List<Object>> countryMap = new TreeMap<>();//sorteerib automaatselt tähestiku järgi

            countryMap.put("Eesti", Arrays.asList("Tallinn", "Jüri Ratas", Arrays.asList("eesti", "vene")));
            countryMap.put("Läti", Arrays.asList("Riia", "Läti Peaminister", Arrays.asList("läti", "vene")));
            countryMap.put("Leedu", Arrays.asList("Vilnius", "Leedu Peaminister", Arrays.asList("leedu", "vene")));
            countryMap.put("Saksamaa", Arrays.asList("Berliin", "Saksamaa Peaminister", Arrays.asList("saksa", "türgi")));
            countryMap.put("Itaalia", Arrays.asList("Rooma", "Itaalia Peaminister", Arrays.asList("itaalia", "saksa")));
            countryMap.put("Prantsusmaa", Arrays.asList("Pariis", "Prantsuse Peaminister", Arrays.asList("prantsuse", "vene")));
            countryMap.put("Suurbritannia", Arrays.asList("London", "Suurbritannia Peaminister", Arrays.asList("inglise", "iiri", "šoti")));
            countryMap.put("Tšehhi", Arrays.asList("Praha", "Tšehhi Peaminister", Arrays.asList("tšehhi", "slovaki")));
            countryMap.put("Norra", Arrays.asList("Oslo", "Norra Peaminister", Arrays.asList("norra", "saami")));
            countryMap.put("Rootsi", Arrays.asList("Stockholm", "Rootsi Peaminister", Arrays.asList("rootsi", "saami")));

            for (String countryName : countryMap.keySet()) {
                System.out.println("\n" + countryName + ":");
                List<String> languages = (List<String>) countryMap.get(countryName).get(2);
                for (String language : languages) {
                    System.out.print(language + ", ");
                }

            }
        }

        System.out.println();

        /*Ülesanne 11
        Korda eelnevat ülesannet, aga kasuta andmete hoidmiseks Queue-tüüpi elementi.*/

        Queue<List<Object>> countryQueue = new LinkedList<>();
        countryQueue.add(Arrays.asList("Eesti", "Tallinn", "Jüri Ratas", Arrays.asList("eesti", "vene")));
        countryQueue.add(Arrays.asList("Läti", "Riia", "Läti Peaminister", Arrays.asList("läti", "vene")));
        countryQueue.add(Arrays.asList("Leedu", "Vilnius", "Leedu Peaminister", Arrays.asList("leedu", "vene")));
        countryQueue.add(Arrays.asList("Saksamaa", "Berliin", "Saksamaa Peaminister", Arrays.asList("saksa", "türgi")));
        countryQueue.add(Arrays.asList("Itaalia", "Rooma", "Itaalia Peaminister", Arrays.asList("itaalia", "saksa")));
        countryQueue.add(Arrays.asList("Prantsusmaa", "Pariis", "Prantsuse Peaminister", Arrays.asList("prantsuse", "vene")));
        countryQueue.add(Arrays.asList("Suurbritannia", "London", "Suurbritannia Peaminister", Arrays.asList("inglise", "iiri", "šoti")));
        countryQueue.add(Arrays.asList("Tšehhi", "Praha", "Tšehhi Peaminister", Arrays.asList("tšehhi", "slovaki")));
        countryQueue.add(Arrays.asList("Norra", "Oslo", "Norra Peaminister", Arrays.asList("norra", "saami")));
        countryQueue.add(Arrays.asList("Rootsi", "Stockholm", "Rootsi Peaminister", Arrays.asList("rootsi", "saami")));

        while (!countryQueue.isEmpty()) {
            List<Object> country11 = countryQueue.remove();
            System.out.println(country11.get(0) + ": ");
            for (String language11 : (List<String>) country11.get(3)) {
                System.out.print("*" + language11 + " ");
            }
            System.out.println();
        }


    }
}


