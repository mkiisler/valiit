public class Day3ForEach {

    public static void main(String[] args) {

        String[] texts = {"Tallinn", "Tartu", "Narva"};
        for (String temporaryTextValue : texts) {
            System.out.println(temporaryTextValue + " on Eesti linn.");
        }


    }
}
