import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Song {

//        Ülesanne 5
//        Defineeri klass Song, millel oleksid järgmised atribuudid:
//        title - muusikapala nimi
//        artist - esitaja nimi
//        album - album, kust lugu pärineb
//        released - väljaandmise aeg (aasta täpsusega, täisarv)
//        genres - žanrid, mille alla antud muusikapala võib liigitada (peaks olema kas massiiv, list või midagi sarnast)
//        lyrics - laulusõnad (tekst)
//
//        Kõik eelnevalt loetletud atribuudid peavad olema esindatud nii setX- kui ka getX-meetoditega.
//
//        Override’i selle klassi toString() meetod nii, et selle välja kutsudes väljastatakse kena ja hästi formaaditud tekst koos kõigi laulu atribuutidega.
//
//        Tekita main()-meetodis antud klassist üks objekt, väärtusta kõik selle objekti atribuudid ja prindi selle objekti info standardväljundisse toString() meetodi abil.


    private String title;
    private String artist;
    private String album;
    private int released;
    private String[] genres;
    private String lyrics;

    public Song(String title, String artist, String album, int released, String[] genres, String lyrics) {
        this.title = title;
        this.artist = artist;
        this.album = album;
        this.released = released;
        this.genres = genres;
        this.lyrics = lyrics;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public int getReleased() {
        return released;
    }

    public void setReleased(int released) {
        this.released = released;
    }

    public String[] getGenres() {
        return genres;
    }

    public void setGenres(String[] genres) {
        this.genres = genres;
    }

    public String getLyrics() {
        return lyrics;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    @Override
    public String toString() {

//        StringBuilder allThisSongGenres = new StringBuilder();
//        for (int i = 0; i < getGenres().length; i++) {
//            allThisSongGenres.append(getGenres()[i] + ", ");
//        }

        String allThisSongGenres = Arrays.toString(getGenres());

        return "Pealkiri: " + getTitle() + "\nEsitaja: " + getArtist() + "\nAlbum: " + getAlbum() + "\nVäljaandmise aasta: "
                + getReleased() + "\nŽanr: " + allThisSongGenres + "\nLaulusõnad:\n" + getLyrics();
// ??? miks ei prindi žanri?
    }

}

