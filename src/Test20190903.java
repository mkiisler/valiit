import java.util.ArrayList;
import java.util.List;

public class Test20190903 {

    public static void main(String[] args) {

//        Ülesanne 1 - töötab
//        Deklareeri muutuja, mis hoiaks endas Pi (https://en.wikipedia.org/wiki/Pi) arvulist väärtust vähemalt 11
//        kohta peale koma. Korruta selle muutuja väärtus kahega ja prindi standardväljundisse.

        double pi = 3.14159265358;
        System.out.println(pi * 2);
// variant
        double pi2 = Math.PI;
        System.out.println(pi2 * 2);

        System.out.println(); // vaherida

//        Ül 2
//  ... Kutsu see meetod main()-meetodist välja ja prindi tulemus standardväljundisse.
        System.out.println(calculateAreaOfRectangle(2, 2));

        System.out.println(); // vaherida

//        Ül 3
//  ...   Kutsu see meetod main()-meetodist välja ja prindi tulemus standardväljundisse (tsükli abil).

        String[] inputArray = {"auto", "rebane", "sau"};
        System.out.println(changeLettersToHashMarks(inputArray));

        System.out.println(); // vaherida

        // ÜL 3 õpetaja klassi lahendus
        String[] words = {"auto", "rebane", "sau"};
        String[] censoredWords = x(words);


        System.out.println(); // vaherida


//        Ül 4
//  ...  Kutsu see meetod main()-meetodist välja ja prindi tulemus standardväljundisse järgmiste sisendparameetrite korral: 25, 31, 6, -4, 11, 14, 17, 22.

        System.out.println(describeWether(25));
        System.out.println(describeWether(31));
        System.out.println(describeWether(6));
        System.out.println(describeWether(-4));
        System.out.println(describeWether(11));
        System.out.println(describeWether(14));
        System.out.println(describeWether(17));
        System.out.println(describeWether(22));

        System.out.println(); // vaherida

//        Ülesanne 5
//        Defineeri klass Song, millel oleksid järgmised atribuudid:
//        title - muusikapala nimi
//        artist - esitaja nimi
//        album - album, kust lugu pärineb
//        released - väljaandmise aeg (aasta täpsusega, täisarv)
//        genres - žanrid, mille alla antud muusikapala võib liigitada (peaks olema kas massiiv, list või midagi sarnast)
//        lyrics - laulusõnad (tekst)
//
//        Kõik eelnevalt loetletud atribuudid peavad olema esindatud nii setX- kui ka getX-meetoditega.
//
//        Override’i selle klassi toString() meetod nii, et selle välja kutsudes väljastatakse kena ja hästi formaaditud
//        tekst koos kõigi laulu atribuutidega.
//
//        Tekita main()-meetodis antud klassist üks objekt, väärtusta kõik selle objekti atribuudid ja prindi selle objekti
//        info standardväljundisse toString() meetodi abil.

        /* Ma ei tea, kuidas väärtustada massiivi ja listi niimoodi konstruktoriga.*/

        Song song1 = new Song("Kati karu", "A. Ratassepp", "Entel-tentel", 1971,
                new String[]{"lastelaulud", "vokaalmuusika"},
                "\tKas teist keegi aru sai,\n\tkuhu Kati karu sai?\n\tVist on karu salamahti\n\tteinud lihtsalt ukse lahti,\n\tvist on keset mängukära\n\tkaru läinud lihtsalt ära.");

        System.out.println(song1.toString());

    }


//    Ülesanne 2 - töötab
//    Kirjuta meetod, mis tagastab ristküliku pindala täisarvulisel kujul ning mille sisendparameetriteks on kaks täisarvu (ristküliku küljepikkused).
//    Meetodi nime võid ise välja mõelda.
//    Kutsu see meetod main()-meetodist välja ja prindi tulemus standardväljundisse.

    public static int calculateAreaOfRectangle(int side1, int side2) {
        return side1 * side2;
    }

// Ül 3
//        Kirjuta meetod, mille sisendparameetriks on stringide massiiv ja mis tagastab stringide massiivi.
//        Tagastatava massiivi iga element sisaldab nii palju #-märke, kui palju oli tähti sisendmassiivi vastava elemendi sõnas.
//                Näiteks:
//        Sisendmassiiv: {“auto”, “rebane”, “sau”}
//        Tagastatav massiiv: {“####”, “######”, “###”}
//
//        Meetodi nime võid ise välja mõelda.
//        Kutsu see meetod main()-meetodist välja ja prindi tulemus standardväljundisse (tsükli abil).

    public static String changeLettersToHashMarks(String[] inputArray) {
        int i = 0;
        String hashMarksArray = "";
        for (; i < inputArray.length; i++) {
//
            for (int j = 0; j < inputArray[i].length(); j++) {
                hashMarksArray = hashMarksArray + "#";
            }
            if (i < inputArray.length - 1) {
                hashMarksArray = hashMarksArray + ", ";
            }

        }
        return (hashMarksArray);
    }

//    public static String [] changeLettersToHashMarks2(String[] inputArray2) {
//        int i = 0;
//        String hashMarksArray2 = "";
//        for (; i < inputArray2.length; i++) {
////
//            for (int j = 0; j < inputArray2[i].length(); j++) {
//                hashMarksArray2 = hashMarksArray2 + "#";
//            }
//            if (i < inputArray2.length - 1) {
//                hashMarksArray2 = hashMarksArray2 + ", ";
//            }
//
//        }
//        return (hashMarksArray2);
//    }

    // ÜL 3 õpetaja klassi lahendus

    public static String[] x(String[] input){
        String[] result = new String[input.length];
        for (int i = 0; i < input.length; i++){
            String tmp = "#".repeat(input[i].length());
            result[i] = tmp;
        }


        return result;
    }



//        Ülesanne 4 - töötab
//        Kirjuta meetod, mis tagastaks inimkeeli arusaadava kirjelduse etteantud õhutemperatuuriga ilmale.
//        Sisendparameeter on täisarv (temperatuur kraadides), väljundiks on ilma kirjeldus teksti kujul.
//                Õhutemperatuurivahemikele vastav ilmakirjeldus on järgmine:
//        Üle 30 kraadi → “Leitsak”
//        25 - 30 kraadi → “Rannailm”
//        21 - 25 kraadi → “Mõnus suveilm”
//        15 - 21 kraadi → “Jahe suveilm”
//        10 - 15 kraadi → “Jaani-ilm”
//        0 - 10 kraadi → “Telekavaatamise ilm”
//        Alla 0 kraadi → “Suusailm”
//
//        Meetodi nime võid ise välja mõelda.
//        Kutsu see meetod main()-meetodist välja ja prindi tulemus standardväljundisse järgmiste sisendparameetrite korral: 25, 31, 6, -4, 11, 14, 17, 22.

    public static String describeWether(int temp) {

        if (temp > 30) {
            return "Leitsak";
        } else if (temp >= 25 && temp <= 30) {
            return "Rannailm";
        } else if (temp >= 21 && temp < 25) {
            return "Mõnus suveilm";
        } else if (temp >= 15 && temp < 21) {
            return "Jahe suveilm";
        } else if (temp >= 10 && temp < 15) {
            return "Jaaniilm";
        } else if (temp >= 0 && temp < 10) {
            return "Telekavaatamise ilm";
        } else if (temp < 0) {
            return "Suusailm";
        } else {
            return "See ei saa olla õhutemperatuur!";
        }


    }
}




