public class Day2If {
    public static void main(String[] args) {
        int temp = Integer.parseInt(args[0]);

        //Variant 1
        if (temp > 30) {
            System.out.println("Ilm on ilus!");
        } else if (temp >= 20) {
            System.out.println("Ilm on normaalne.");
        } else if (temp >= 15 && temp <= 20) {
            System.out.println("On jaanipäev.");
        } else {
            System.out.println("Ilm ei ole ilus!");
        }
        // Variant 2
        // 1 - roheline tuli
        // 2 - kollane tuli
        // 3 - punane tuli

        int inputValue = 0;

        switch(inputValue) {
            case 1:
                System.out.println("Roheline tuli!");
                break;
            case 2:
                System.out.println("Kollane tuli!");
                break;
            case 3:
                System.out.println("Punane tuli!");
                break;
            default:
                System.out.println("Rikkis!");
        }

        // Variant 3 (inline-if)
        // String muutuja = Kas tingmus on tõene ? ""jah" : "ei";
        String weatherText = temp >= 30 ? "Ilm on ilus." : "Ilm ei ole ilus.";
            System.out.println(weatherText);
    }
}
