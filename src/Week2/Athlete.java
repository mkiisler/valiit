package Week2;

public abstract class Athlete { // abstaktsest klassist pole võimalik objekti luua // ülemklass ei pea olema alati abstraktne
            // mitteabstraktsest klassist pärinemise korral ei pea implementeerima meetodeid

    public String firstName;
    public String lastName;
    public int age;
    public String gender;
    public int height;
    public double weight;

////    public Athlete(){}
//
//    public Athlete(String firstName, ) // Mingi superkonstruktor, vt õpetaja faili

    public abstract void perform(); // abstaktsel meetodidl pole keha, on ainult signatuur ja kasutaja peab selle defineerima


}


