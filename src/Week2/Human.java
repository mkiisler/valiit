package Week2;

public class Human {

    public String name; // public tähendab, et on väljastpoolt klassi ligipääsetav, aga muutijaid ei defineerita public'una
    public int age;
    public double weight;


    public static int humanCount = 0;

    // loome konstruktor-tüüpi meetodi nimi, peab olema sama nagu klassil. Selline konstruktor luuakse niikuinii:

    public Human() { // vaikimisi konstruktor tuleb luua, kui lood kasvõi ühe konstruktori

    }

    // loome konstruktor-tüüpi meetodi oma parameetritega

    public Human(String name, int age, double weight) {
        this.name = name; // this.name viitab klassi muutujale eespool
        this.age = age;
        this.weight = weight; // objektimuutujaid võb olla palju - igal objektil oma koopia
        Human.humanCount++; // klassimuujuaid on ainult 1 ex

    }

    public Human(String name) {  // uus abikonstruktor, kui ei tea vanust ja kaalu
        this.name = name;
        Human.humanCount++;


    }


    public boolean isYoung() { // kuna meetod on klassi sees, siis pääseb selle klassi atribuutitele ligi
        // mitte-static-lemmendid on instruktsioonid
        return this.age < 100; // this.  - konkreetselt selle objekti muutuja


    }

    public static boolean isThisHumanYoung(Human x) { //Sulgudes "Human" on klassi nimi, "person" on)
        return x.age < 100;

    }


}

