package Week2;

public class Day07 {

    public static void main(String[] args) {

        System.out.println("Inimeste arv: " + Human.humanCount);

        Human h1 = new Human("Ragnar");
        changeHumanName(h1); // objektide puhul antakse viide, objektist koopiat ei tehta
        System.out.println(h1.name);

        System.out.println("Inimeste arv: " + Human.humanCount);

        int myNumber = 222;
        changePrimitiveNumber(myNumber); // primitiivse muutuja väärtusest tehakse koopia, seda ei muudeta
        System.out.println(myNumber);

        Integer myNumber2 = 222;
        changePrimitiveNumber2(myNumber2); // Kui vaja muuta, siis teha eraldi klass. Praegu jääb 222
        System.out.println(myNumber2);
    }

    private static void changeHumanName(Human x) {
        x.name = "Kris";
    }

    private static void changePrimitiveNumber(Integer inputNumber) {
        inputNumber = 555;
    }

    private static void changePrimitiveNumber2(Integer inputNumber) {
        inputNumber = 555;
    }
}
