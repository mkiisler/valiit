package Week2;

public class Day06uus {

    public static void main(String[] args) {

        // meetodi väljakutsusmine

        int birthYear1 = retrieveBirthYear("38104...");
        int birthYear2 = retrieveBirthYear("47208...");

        System.out.println(birthYear1);

        System.out.println(retrieveBirthYear("47208..."));

        System.out.println("Korrektne isikukood: " + isPersonalCodeCorrect("38104242729"));
        System.out.println("Korrektne isikukood: " + isPersonalCodeCorrect("47208032754"));
        System.out.println("Korrektne isikukood: " + isPersonalCodeCorrect("48207212727"));
        System.out.println("Korrektne isikukood: " + isPersonalCodeCorrect("8"));
        System.out.println("Korrektne isikukood: " + isPersonalCodeCorrect("01234567899"));

    }
      /*
        Tekitada Java projekt ja kirjutada meetod, mis võtab sisendparameetrina vastu Eesti residendi isikukoodi
        String tüüpi väärtusena ja tagastab selle põhjal antud isiku sünniaasta täisarvulisel kujul.

        Tekitada Java projekt ja kirjutada meetod, mis võtab sisendparameetrina vastu Eesti residendi isikukoodi
        String tüüpi väärtusena ja tagastab boolean tüüpi väärtuse, mis määratleb, kas isikukoodi
        kontrollnumber oli korrektne või mitte

         */

    private static int retrieveBirthYear(String personalCode1) {
        //38104242779

        int centuryKey = Integer.parseInt(personalCode1.substring(0, 1));
        int centuryYear = Integer.parseInt(personalCode1.substring(1, 3));

        int century;
        switch (centuryKey) {
            case 1:
            case 2:
                century = 1800;
                break;
            case 3:
            case 4:
                century = 1900;
                break;
            case 5:
            case 6:
                century = 2000;
                break;
            case 7:
            case 8:
                century = 2100;
                break;
            default:
                century = 0;

        }
        return century + centuryYear;
    }

    private static boolean isPersonalCodeCorrect(String personalCode) {

        if (personalCode == null || personalCode.length() != 11) {
            return false;

        }

        // Muudame isikkukoodi teksti täisarvude massiiviks

        int[] personalCodeDigits = new int[11];

        char[] personalCodeChars = personalCode.toCharArray();

        for (int i = 0; i < 10; i++) {
            //personalcode to CharArray
            // '3' --> "3" --> parse --> 3
            String digitString = String.valueOf(personalCodeChars[i]);
            int digit = Integer.parseInt(digitString);
            personalCodeDigits[i] = digit;

        }

        int[] weights1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1};

        int sum = 0;

        for (int i = 0; i < 10; i++) {
            sum = sum + weights1[i] * personalCodeDigits[i];


        }

        int chechNumber = 0;
        if (sum % 11 != 10) {
            chechNumber = sum % 11;

        } else {
            int[] weights2 = {3, 4, 5, 6, 7, 8, 9, 1, 2, 3};
            sum = 0;
            for (int i = 0; i < 10; i++) {
                sum = sum + weights2[i] * personalCodeDigits[i];

            }
            if (sum % 11 != 10) {
                chechNumber = sum % 11;

            } else {
                chechNumber = 0;
            }
        }

        char personalCodeLastChar = personalCode.charAt(10);
        String personalCodeLastCharStr = String.valueOf(personalCodeLastChar);
        int personalCodeLastDigit = Integer.parseInt(personalCodeLastCharStr);

        boolean isCorrect = personalCodeLastDigit == chechNumber;
        return isCorrect;

    }

}

