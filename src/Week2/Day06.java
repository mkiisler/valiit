package Week2;

import java.math.BigDecimal;
import java.math.BigInteger;

public class Day06 {
    public static void main(String[] args) {
        // kordamisharjutus
        /*
        Ülesanne 1:
● Defineeri muutuja, mis hoiaks väärtust 456.78.
● Defineeri muutuja, mis viitaks tähtede jadale “test”
● Defineeri muutuja, mis hoiaks tõeväärtust, mis viitaks
avaldise tõesusele
● Defineeri muutuja, mis viitaks numbrite kogumile 5, 91, 304,
405.
● Defineeri muutuja, mis viitaks numbrite kogumile 56.7, 45.8.
91.2
● Defineeri kaks erinevat tüüpi muutujat, mis hoiaksid endas
väärtust ‘a’.
● Defineeri muutuja, mis viitaks järgmiste väärtuste kogumile:
“see on esimene väärtus”, “67”, “58.92”.
● Defineeri muutuja, mis viitaks väärtusele
7676868683452352345324534534523453245234523452345234523452345
ja mis ei oleks String-tüüpi.
         */


        double d1 = 456.78;
        double d2 = 456.78d;
        float f1 = 456.78f;

        String t1 = "test";
        char[] ca1 = {'t', 'e', 's', 't'};

        boolean b1 = true;
        boolean b2 = 5 > 3 && 2 < 1001 || 7 == 8;

        // lühem viis massiivi täitmiseks
        int[] ia1 = {5, 91, 304, 405};

        // pikem viis masiivi täitmiseks
        double[] da1 = new double[3];
        da1[0] = 56.7;
        da1[1] = 45.8;
        da1[2] = 91.2;

        char charA1 = 'a';
        short charA2 = 'a'; // !!! Vaata short üle
        String stringA = "a";

        String[] sa1 = {"see on esimene väärtus", "67", "58.92"};

        // BigDecimal ja BigInteger
        BigInteger bignumber = new BigInteger("7676868683452352345324534534523453245234523452345234523452345");

        bignumber = bignumber.multiply(new BigInteger("2")); // suure numbri korrutamine

        BigDecimal bi2 = new BigDecimal("7676868683452352345324534534523453245234523452345234523452345");



    }
}
