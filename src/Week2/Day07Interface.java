package Week2;

public interface Day07Interface { // Car klassi välimus. Defineerib ainult meetodi signatuurid.
    public void drive();
    public void speedUp();
    public void stop();
    public void addDriver(Human driver);


}


// public class Ferrari implements Day07Interface