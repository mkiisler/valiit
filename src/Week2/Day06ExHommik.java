package Week2;

public class Day06ExHommik {
    public static void main(String[] args) {

        // 1. ja 2. ülesande funktsiooni väljakutsumine

        test1(3);
        test2(null, "mingi tekst");

        // 5. ülesande funktsiooni väljakutsumine ja väljaprintimine

        String gender = deriveGender​("47208032754");
        System.out.println("Sugu: " + gender);

// 6. ülesande funktsiooni väljakutsumine
        printHello();


    }


    /*
    Ülesanne 1
Defineeri meetod nimega test​, mis võtab sisendparameetrina
täisarvu ja tagastab tõeväärtuse.

Kutsu see meetod main() meetodist välja.*/

    static boolean test1(int someNumber) {
        return false;

    }


    /*
Ülesanne 2
Defineeri meetod nimega test2​, millel kaks String-tüüpi
sisendparameetrit ja mis ei tagasta mitte midagi.

Kutsu see meetod main() meetodist välja.
*/

    static void test2(String text1, String text2) {


    }


/*
Ülesanne 3
Defineeri meetod nimega addVat​, mis võtab sisendparameetrina ühe
double-tüüpi muutuja ja tagastab double-tüüpi muutuja.

Kutsu see meetod main() meetodist välja.    */

    static double addVat(double initialPrice) {
        return initialPrice * 1.2;

    }

/*
Ülesanne 4
Defineeri meetod, mis võtab sisendparameetritena kaks täisarvu
ja ühe tõeväärtuse ning tagastab täisarvude massiivi. Nime võid
ise välja mõelda.

Kutsu see meetod main() meetodist välja.*/

    static int[] generateArray(int a, int b, boolean alpha) {
        if (alpha) {
            return new int[]{a, b};

        } else {
            return new int[]{b, a};
        }

    }



/*
Ülesanne 5
Defineeri meetod deriveGender​, mis võtab sisendparameetrina
vastu Eesti residendi isikukoodi tekstikujul ja tagastab
kõnealuse isikukoodi omaniku soo tekstilisel kujul (kas “M” või
“F”).

Kutsu see meetod main() meetodist välja ja talleta tulemus
lokaalses main() meetodi muutujas nimega gender​. Prindi selle
muutuja väärtus standardväljundisse.  */


    static String deriveGender​(String personalCode) {
        char firstDigetChar = personalCode.charAt(0);
        String firstDigitStr = String.valueOf(firstDigetChar);
        int firstDigitValue = Integer.parseInt(firstDigitStr);

        return firstDigitValue % 2 == 1 ? "M" : "F"; // true / False

//        if (firstDigitValue % 2 == 1){    // see on pikem variant
//            return "M";
//        }else {
//            return "F";
//        }

    }
    /*
    Ülesanne 6

Defineeri meetod printHello​, mis ei võta vastu ühtki
sisendparameetrit ning mis ei tagasta ühtki väärtust, aga
prindib standardväljundisse “Tere”.

Kutsu see meetod main() meetodist välja.
     */

    static void printHello() {
        System.out.println("Tere");
    }


}

