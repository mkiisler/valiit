package Week2;

public class Country {

    public String name;
    public String capital;
    public String primeMinister;
    public String[] languages;

    public Country(String name, String capital, String primeMinister, String[] languages) {
        this.name = name;
        this.capital = capital;
        this.primeMinister = primeMinister;
        this.languages = languages;
    }

    @Override
    public String toString() {
        String info = this.name + ":\n";
        for (String languages : this.languages) {
            info = info + "\t" + languages + "\n";
        }
        return info;

    }


}

    /*
    Ülesanne 7

        Tekita kahetasandiline massiiv, mis hoiaks infot riigi, selle pealinna, ja peaministri nimega (massiivi minimaalsed mõõtmed: 10X3) Näiteks:
        Estonia, Tallinn, Jüri Ratas
        Latvia, Riga, Arturs Krišjānis Kariņš
...


        Trüki välja ainult riikide peaministrid - iga nimi eraldi real.
                Trüki välja iga riigi kohta rida: Country: XXX, Capital: YYY, Prime minister: ZZZ

    String[][] riikPealinnPeaminister = new String[10][3];
    riikPealinnPeaminister[0] = new String[]{"Eesti", "Tallinn", "Jüri Ratas"};
    riikPealinnPeaminister[1] = new String[]{"Läti", "Riia", "Läti Peaminister"};
    riikPealinnPeaminister[2] = new String[]{"Leedu", "Vilnius", "Leedu Peaminister"};
    riikPealinnPeaminister[3] = new String[]{"Saksamaa", "Berliin", "Saksamaa Peaminister"};
    riikPealinnPeaminister[4] = new String[]{"Itaalia", "Rooma", "Itaalia Peaminister"};
    riikPealinnPeaminister[5] = new String[]{"Prantsusmaa", "Pariis", "Prantuse Peaminister"};
    riikPealinnPeaminister[6] = new String[]{"Suurbritannia", "London", "Suurbritannia Peaminister"};
    riikPealinnPeaminister[7] = new String[]{"Tšehhi", "Praha", "Tšehhi Peaminister"};
    riikPealinnPeaminister[8] = new String[]{"Norra", "Oslo", "Norra Peaminister"};
    riikPealinnPeaminister[9] = new String[]{"Rootsi", "Stockholm", "Rootsi Peaminister"};

    String[][] riikPealinnPeaminister2 = {{"Eesti", "Tallinn", "Jüri Ratas"},
            {"Läti", "Riia", "Läti Peaminister"},
            {"Leedu", "Vilnius", "Leedu Peaminister"},
            {"Saksamaa", "Berliin", "Saksamaa Peaminister"},
            {"Itaalia", "Rooma", "Itaalia Peaminister"},
            {"Prantsusmaa", "Pariis", "Prantuse Peaminister"},
            {"Suurbritannia", "London", "Suurbritannia Peaminister"},
            {"Tšehhi", "Praha", "Tšehhi Peaminister"},
            {"Norra", "Oslo", "Norra Peaminister"},
            {"Rootsi", "Stockholm", "Rootsi Peaminister"}};

        for (String[] country : riikPealinnPeaminister) {
        System.out.println(country[2]);
    }

        for (int i = 0; i < riikPealinnPeaminister2.length; i++) {
        System.out.println(String.format("Country: %s, Capital: %s, Prime minister: %s",
                riikPealinnPeaminister2[i][0], riikPealinnPeaminister2[i][1], riikPealinnPeaminister2[i][2]));
    }

     */


