package Week2;

public class Person {

    /*
    Kirjutada Java klass Person, mille konstruktor võtab sisendparameetrina vastu Eesti residendi isikukoodi
Selle klassi baasil loodud objektil on järgmised meetodid:
getBirthYear (tagastab täisarvu)
getBirthMonth (tagastab teksti)
getBirthDayOfMonth (tagastab sünnikuupäeva)
getGender (tagastab Enum tüüpi muutuja)

     */

    private String personalCode;

    public Person(String personalCode) {
        this.personalCode = personalCode;
    }

    // poolik, vaata õpetaja failist.


    private static int retrieveBirthYear(String personalCode1) {
        //38104242779

        int centuryKey = Integer.parseInt(personalCode1.substring(0, 1));
        int centuryYear = Integer.parseInt(personalCode1.substring(1, 3));

        int century;
        switch (centuryKey) {
            case 1:
            case 2:
                century = 1800;
                break;
            case 3:
            case 4:
                century = 1900;
                break;
            case 5:
            case 6:
                century = 2000;
                break;
            case 7:
            case 8:
                century = 2100;
                break;
            default:
                century = 0;

        }
        return century + centuryYear;
    }
//
//    public String gerBirthMonth() {
//
//    }
//
//
//    private static boolean isPersonalCodeCorrect(String personalCode) {
//
//    }
}

