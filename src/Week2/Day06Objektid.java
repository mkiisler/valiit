package Week2;

public class Day06Objektid {

    public static void main(String[] args) {

        Human rein = new Human(); // kui konstruktor on loodud, siis nii enam teha ei saa
        rein.name = "Rein";
        rein.age = 45;
        rein.weight = 87;

        Human rein2 = new Human("Rein", 45, 87);

//        Human mari = new Human();
//        mari.name = "Mari";
//        mari.age = 28;
//        mari.weight = 55;

        Human mari = new Human("Mari", 28, 55);

        Human toomas = new Human("Toomas");
        System.out.println("Mari on noor? " + mari.isYoung());
        System.out.println("Kas Mari on noor? " + Human.isThisHumanYoung(mari)); // see on korrektne viis
        System.out.println("Kas Mari on ikka noor? " + mari.isThisHumanYoung(mari)); // mari. on vale !!!


    }
}
