package Week2;

import java.util.ArrayList;
import java.util.List;

public class Day06ExObjektKonstruktor {


    public static void main(String[] args) {
        List<Country> countries = new ArrayList<>();

        // pikem vii lisada andmeid
        Country estonia = new Country("Estonia", "Tallinn", "Jüri Ratas", new String[]{"eesti", "vene", "inglise"});
        countries.add(estonia);

        // lühem viis lisada andmeid
        countries.add(new Country("Latvia", "Riia", "Läti Peaminister", new String[]{"läti", "vene", "leedu"}));
        countries.add(new Country("Rootsi", "Stockholm", "Rootsi Peaminister", new String[]{"rootsi", "soome"}));

        for (Country country : countries) {
            System.out.println(country);


//            System.out.println(country.name + ":");
//            for (String language : country.languages) {
//                System.out.println(" " + language);
//            }
        }

        /*
        Ülesanne 1:
a) Defineeri klass Sportlane (Athlete), millel järgmised
i) Omadused:
1) Eesnimi
2) Perenimi
3) Vanus
4) Sugu
5) Pikkus
6) Kaal
ii) Käitumine
1) Spordi (perform) - meetod mille kaudu teeks sportlane
seda, mis on tema spordiala

b) Defineeri kaks alamklassi, mis pärineksid Athlete klassis:
i) Skydiver
ii) Runner
c) Kirjuta üle (override’i) tuletatud klasside perform() meetod vastavalt
sellele, mis tüüpi sportlasega on tegemist.
d) Loo 3 objekti - 3 langevarjurit ja 3 jooksjat. Väärtusta nende
atribuutide väärtused.
e) Prindi need objektid ükshaaval standardväljundisse (atribuut
haaval).
f) Käivita sportlaste perform() meetodid main() meetodi seest.
         */

        List<Athlete> athletes = new ArrayList<>();

        Runner runner1 = new Runner();
        runner1.firstName = "Thomas";
        runner1.lastName = "More";

        Runner runner2 = new Runner();
        runner2.firstName = "Elon";
        runner2.lastName = "Musk";

        Runner runner3 = new Runner();
        runner3.firstName = "Bill";
        runner3.lastName = "Gates";


        Skydiver skydriver1 = new Skydiver();
        skydriver1.firstName = "Donald";
        skydriver1.lastName = "Trump";

        athletes.add(runner1);
        athletes.add(runner2);
        athletes.add(runner3);
        athletes.add(skydriver1);

        athletes.get(1).perform();
        athletes.get(3).perform();




    }
}



    /*
    Kirjutada Java klass Person, mille konstruktor võtab sisendparameetrina vastu Eesti residendi isikukoodi
Selle klassi baasil loodud objektil on järgmised meetodid:
getBirthYear (tagastab täisarvu)
getBirthMonth (tagastab teksti)
getBirthDayOfMonth (tagastab sünnikuupäeva)
getGender (tagastab Enum tüüpi muutuja)

     */


