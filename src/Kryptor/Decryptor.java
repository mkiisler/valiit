package Kryptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Decryptor extends Cryptor {

    @Override
    public void initAlphabet(List<String> alphabet) {
        for (String line : alphabet) {
            String[] lineParts = line.split(", ");
            this.cryptoMap.put(lineParts[1], lineParts[0]);

        }

    }


}
