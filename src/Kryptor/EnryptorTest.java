package Kryptor;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class EnryptorTest {

    private List<String> alphabet;

    @BeforeEach

    public void setup() {
        // String "J, K" --> Map J -> K
        // String "A, E" --> Map A -> E
        this.alphabet = Arrays.asList("J, K", "A, E");
    }

    @AfterEach
    public void teadDown() {

    }

    @Test
    public void testInitAlphabet() {
        Encryptor encryptor = new Encryptor();
        encryptor.initAlphabet(alphabet);
        Assertions.assertTrue(encryptor.cryptoMap.get("J").equals("K"));
        Assertions.assertTrue(encryptor.cryptoMap.get("A").equals("E"));
    }
}
