package Kryptor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class CryptoApp {

    public static void main(String[] args) throws IOException {

        List<String> alphabet = Files.readAllLines(Paths.get(args[0]));

        Cryptor encryptor = new Encryptor();
        encryptor.initAlphabet(alphabet);
        String encryptedMessage = encryptor.convertText(args[1].toUpperCase());
        System.out.println("Krüpteeritud sõnum: " + encryptedMessage);

        Cryptor decryptor = new Decryptor();
        decryptor.initAlphabet(alphabet);
        String decryptedMessage = decryptor.convertText(args[2]);
        System.out.println("Dekrüpteeritud sõnum: " + decryptedMessage);


    }
}
