package Kryptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Cryptor {

    protected Map<String, String> cryptoMap = new HashMap<>();

    public abstract void initAlphabet(List<String> alphabet);

    public String convertText(String textToConvert) {
        String convertedText = "";
        char[] textChars = textToConvert.toCharArray();

        for (int i = 0; i < textChars.length; i++) {
            String charString = String.valueOf(textChars[i]);
            convertedText = convertedText + this.cryptoMap.get(charString);
//            convertedText++ this.cyptoMap.get(charString);
        }

        return convertedText;
    }


}
