public class Day3ArrayEx {

    public static void main(String[] args) {

        /*Ülesanne 7:
● Defineeri muutuja, mille tüübiks täisarvude massiiv.
● Loo massiiv pikkusega 5 ja pane oma eelnevalt defineeritud
        muutuja sellele viitama.
● Sisesta ükshaaval massiivi väärtused 1, 2, 3, 4, 5
● Prindi standardväljundisse esimese massiivielemendi väärtus.
● Prindi standardväljundisse kolmanda massiivielemendi väärtus.
● Prindi standardväljundisse viimase massiivielemendi väärtus.*/

        int[] arv;
        arv = new int[5];

        arv[0] = 1; // !!!
        arv[1] = 2;
        arv[2] = 3;
        arv[3] = 4;
        arv[4] = 5;

        System.out.println(arv[0]);
        System.out.println(arv[2]);
        System.out.println(arv[arv.length - 1]); // !!!

       /* Ülesanne 8:
● Defineeri muutuja, mille tüübiks tekstide massiiv.
● Väärtusta see massiiv loomise hetkel väärtustega “Tallinn”,
“Helsinki”, “Madrid”, “Paris”.*/

        String[] linnad = {"Tallinn", "Helsinki", "Madrid", "Paris"};


       /* Ülesanne 9:
● Defineeri kahetasandliline massiiv:
○ Element 1: 1, 2, 3

○ Element 2: 4, 5, 6
○ Element 3: 7, 8, 9, 0*/

        int[][] kaksTasandit = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9, 0}};

        // !!! pikem variant

        int[][] kaksTasandit2 = new int[3][];

        kaksTasandit2[0] = new int[3];
        kaksTasandit2[0][0] = 1;
        kaksTasandit2[0][1] = 2;
        kaksTasandit2[0][2] = 3;

        kaksTasandit2[1] = new int[3];
        kaksTasandit2[1][0] = 4;
        kaksTasandit2[1][1] = 5;
        kaksTasandit2[1][2] = 6;

        kaksTasandit2[2] = new int[4];
        kaksTasandit2[2][0] = 7;
        kaksTasandit2[2][1] = 8;
        kaksTasandit2[2][2] = 9;
        kaksTasandit2[2][3] = 0;






    /*Ülesanne 10:
● Defineeri kahetasandliline massiiv, milles hoida järgmisi
linnu riikide kaupa:
○ Tallinn, Tartu, Valga, Võru
○ Stockholm, Uppsala, Lund, Köping
○ Helsinki, Espoo, Hanko, Jämsä
● Väärtusta see massiiv kahel erineval viisil: element-haaval,
inline-põhimõttel*/

        String[][] linnadRiikideKaupa1 = new String[3][]; // !!!
        linnadRiikideKaupa1[0][0] = "Tallinn";
        linnadRiikideKaupa1[0][1] = "Tartu";
        linnadRiikideKaupa1[0][2] = "Valga";
        linnadRiikideKaupa1[0][3] = "Võru";
        linnadRiikideKaupa1[1][0] = "Stockholm";
        linnadRiikideKaupa1[1][1] = "Uppsala";
        linnadRiikideKaupa1[1][2] = "Lund";
        linnadRiikideKaupa1[1][3] = "Köping";
        linnadRiikideKaupa1[2][0] = "Helsinki";
        linnadRiikideKaupa1[2][1] = "Espoo";
        linnadRiikideKaupa1[2][2] = "Hanko";
        linnadRiikideKaupa1[2][3] = "Jämsä";


        String[][] linnadRiikideKaupa2 = {{"Tallinn", "Tartu", "Valga", "Võru"},
                {"Stockholm", "Uppsala", "Lund", "Köping"}, {"Helsinki", "Espoo", "Hanko", "Jämsä"}};

        String[][] linnadRiikideKaupa3 = new String[3][];
        linnadRiikideKaupa3[0] = new String[]{"Tallinn", "Tartu", "Valga", "Võru"};
        linnadRiikideKaupa3[1] = new String[]{"Stockholm", "Uppsala", "Lund", "Köping"};
        linnadRiikideKaupa3[2] = new String[]{"Helsinki", "Espoo", "Hanko", "Jämsä"};


    }
}
