public class Day3For {
    public static void main(String[] args) {

        for (int i = 0; i < 100; i++) {
            System.out.println(i);
        }

        int j;
        for (j = 0; j < 100; j++) {
            System.out.println(j);
        }

        int k = 0; //variant, aga kole - ära kasuta
        for (; k < 100;) {
            System.out.println(k);
            k++;
        }

        for (int l = 100; l > 0; l--) {
            System.out.println(l);
        }

        for (int m = 100; m > 0; m-=5) {
            System.out.println(m);
        }

        for (int n = 100; n > 0; n = n - 5) { // eelmise pikem variant sammu defineerimisel
            System.out.println(n);
        }



    }
}
