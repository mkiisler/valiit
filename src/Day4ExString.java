import java.util.Scanner;

public class Day4ExString {
    public static void main(String[] args) {

      /*  Ülesanne 1:
● Defineeri muutujad
○ president1 ​väärtusega “Konstantin Päts”
○ president2 ​väärtusega “Lennart Meri”
○ president3 ​väärtusega “Arnold Rüütel”
○ president4 ​väärtusega “Toomas Hendrik Ilves”
○ president5 ​väärtusega “Kersti Kaljulaid”

● Defineeri String-tüüpi muutuja ja pane see viitama järgmisele
tekstile: “Konstantin Päts, Lennart Meri, Arnold Rüütel,
Toomas Hendrik Ilves ja Kersti Kaljulaid on Eesti
presidendid.”​. Kasuta lause konstrueerimiseks StringBuilder
klassi.
● Prindi tekst standardväljundisse.*/

        String president1 = "Konstatin Päts";
        String president2 = "Lennart Meri";
        String president3 = "Arnold Rüütel";
        String president4 = "Toomas Hendrik Ilves";
        String president5 = "Kersti Kaljulaid";
        StringBuilder sentenceBuilder = new StringBuilder();
        sentenceBuilder.append(president1);
        sentenceBuilder.append(", ");
        sentenceBuilder.append(president2);
        sentenceBuilder.append(", ");
        sentenceBuilder.append(president3);
        sentenceBuilder.append(", ");
        sentenceBuilder.append(president4);
        sentenceBuilder.append(", ");
        sentenceBuilder.append(president5);
        sentenceBuilder.append(" on Eesti presidendid.");
        System.out.println(sentenceBuilder);





/*Ülesanne 2:
● Defineeri String-tüüpi muutuja ja pane see viitama tekstile
“Rida: See on esimene rida. Rida: See on teine rida. Rida:
See on kolmas rida.”
● Kasutades Scanner klassi, prindi standardväljundisse
järgmised read:
See on esimene rida.
See on teine rida.
See on kolmas rida.*/

        Scanner textProcessor = new Scanner("Rida: See on esimene rida. Rida: See on teine rida. Rida: See on kolmas rida.");
        textProcessor.useDelimiter("Rida: "); // .useDelimiter on Scanneri funktsioon
        while (textProcessor.hasNext()) {
            System.out.println(textProcessor.next());
        }

// 2. variant

        String text = "Rida: See on esimene rida. Rida: See on teine rida. Rida: See on kolmas rida.";
        String[] rows = text.split("Rida: ");
        for (String row : rows) {
            System.out.println(row);
        }

        // 3. variant - tavalise for-ga

        for (int i = 0; i < rows.length; i++)    {
            System.out.println(rows[i]);
        }


    }
}
