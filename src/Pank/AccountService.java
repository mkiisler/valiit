package Pank;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class AccountService {


    private static List<Account> accounts = new ArrayList<>();

    public static Account searchAccount(String accountNumber) {
        for (Account account : accounts) {
            if (account.getAccountNumber().equals(accountNumber)) {
                return account;
            }


        }

        return null;
    }

    public static Account searchAccount(String firstName, String lastName) {

        /*for (int i = 0; i < accounts.size(); i++)
            if (accounts.get(i).equals(firstName) && accounts.get(i).equals(lastName)) {
                return accounts.get(i);
            }*/
// Eelmine teeb täpselt sama, mis järgmine.

        for (Account account : accounts) {
            if (account.getFirstName().equals(firstName) && account.getLastName().equals(lastName)) {
                return account;
            }


        }

        return null;
    }

    public static ActionResponse transfer(String fromAccountNumber, String toAccountNumber, double sum) {
        Account fromAccount = searchAccount(fromAccountNumber);
        Account toAccount = searchAccount(toAccountNumber);

        // Kas mõlemad kontos on olemas?
        if (fromAccount == null || toAccount == null) {
            return new ActionResponse(false, null, null, "Vähemalt ühte kontot ei leitud!");
        }

        // Kas kontol on piisavalt raha?
        if (fromAccount.getBalance() < sum) {
            return new ActionResponse(false, fromAccount, toAccount, "Kontol pole piisavalt vahendeid!");
        }

        // Maksja konto muudatused
        double fromAccountBalance = round(fromAccount.getBalance() - sum);
        fromAccount.setBalance(fromAccountBalance);

        // Saaja konto muudatused
        double toAccountBalance = round(toAccount.getBalance() + sum);
        toAccount.setBalance(toAccountBalance);

        return new ActionResponse(true, fromAccount, toAccount, "Ülekanne teostatud");

    }


    public static void loadAccounts(String filePath) throws IOException {
        List<String> accountRows = Files.readAllLines(Paths.get(filePath));

        for (String accountStr : accountRows) {
            String[] parts = accountStr.split(", ");
            String firstName = parts[0];
            String lastName = parts[1];
            String accountNumber = parts[2];
            double balance = Double.parseDouble(parts[3]);
            Account account = new Account(firstName, lastName, accountNumber, balance);
            accounts.add(account);

        }

    }

    // Ümardab kontojäägi

    private static double round(double value){
        return Math.round(value * 100.0) /100.0;
    }

}
