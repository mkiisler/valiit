package Pank;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class PangaApp {

    public static Scanner inputReader = new Scanner(System.in);


    public static void main(String[] args) throws IOException {
        AccountService.loadAccounts(args[0]);

        /*
        Kirjuta main-meetodisse loogika, mis kuulaks konsoolisisendit
(vihje: Scanner). Sisendi formaat võib olla üks järgmistest:
○ TRANSFER <FROM ACCOUNT> <TO ACCOUNT> <SUM>
teostab ülekande, kui võimalik.
○ BALANCE <ACCOUNT> - kuvab kontoandmed
standardväljundisse, kui konto olemas.
○ BALANCE <FIRST NAME> <LAST NAME> - kuvab
kontoandmed standardväljundisse, kui konto olemas.
         */

        while (true) {
            System.out.println();
            System.out.println("Sisesta käsklus: ");
            String command = inputReader.nextLine();
            String[] commandParts = command.split(" ");
            switch (commandParts[0].toUpperCase()) {
                case "BALANCE":
                    displayAccountDetails(commandParts);
                    break;
                case "TRANSFER":
                    doTransfer(commandParts);
                    break;
                case "EXIT": // Main meetod lõpetab tegevuse.

                    return;
                default:
                    System.out.println("Vale käsklus.");
            }

//        while (true) {
//            String accountNumber = inputReader.nextLine();
//            Account account = AccountService.searchAccount2(accountNumber);
//            System.out.println();
//            if (account != null) {
//                System.out.println("KONTO");
//                System.out.println("Name: " + account.getFirstName() + " " + account.getLastName());
//                System.out.println("Kontonumber: " + account.getAccountNumber());
//                System.out.println("Kontojääk: " + account.getBalance());
//            } else {
//                System.out.println("KONTO\nKontot ei leitud");
//
//            }
//        }

        }

//        Account testAccount2 = AccountService.searchAccount("899929247");
//        Account testAccount1 = AccountService.searchAccount("Ora", "Harvey");
//
//        System.out.println(testAccount.getFirstName());
//        System.out.println(testAccount.getAccountNumber());

        /*
        ○ TRANSFER <FROM ACCOUNT> <TO ACCOUNT> <SUM>
teostab ülekande, kui võimalik.
         */

    }

    private static void doTransfer(String[] commandParts) {
        if (commandParts.length == 4) {
            ActionResponse response = AccountService.transfer(commandParts[1], commandParts[2], Double.parseDouble(commandParts[3]));
            System.out.println(response.getMessage());
            if (response.isActionSuccessful()){
                System.out.println("\nMaksja konto andmed: ");
                displayAccountDetails(response.getFronAccount());
                System.out.println("\nSaaja konto andmed: ");
                displayAccountDetails((response.getToAccount()));
            }
        } else {
            System.out.println("Tundmatu käsklus");
        }
    }


    private static void displayAccountDetails(String[] commandParts) {
        if (commandParts.length == 2) {
            Account x = AccountService.searchAccount(commandParts[1]);
            displayAccountDetails(x);
        } else if (commandParts.length == 3) {
            Account y = AccountService.searchAccount(commandParts[1], commandParts[2]);
            displayAccountDetails(y);
        } else {
            System.out.println("Tundmatu käsklus");
        }

    }


    private static void displayAccountDetails(Account accountToDisplay) {
        if (accountToDisplay != null) {
            System.out.println("KONTO");
            System.out.println("Nimi: " + accountToDisplay.getFirstName() + " " + accountToDisplay.getLastName());
            System.out.println("Kontonumber: " + accountToDisplay.getAccountNumber());
            System.out.println("Kontojääk: " + accountToDisplay.getBalance());

        } else {


            System.out.println("Kontot ei leitud");
        }

    }
}
