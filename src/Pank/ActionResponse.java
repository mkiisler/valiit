package Pank;

public class ActionResponse {
    private boolean actionSuccessful;
    private Account fronAccount;
    private Account toAccount;
    private String message;

    public ActionResponse(boolean actionSuccessful, Account fronAccount, Account toAccount, String message) {
        this.actionSuccessful = actionSuccessful;
        this.fronAccount = fronAccount;
        this.toAccount = toAccount;
        this.message = message;
    }

    public boolean isActionSuccessful() {
        return actionSuccessful;
    }

    public Account getFronAccount() {
        return fronAccount;
    }

    public Account getToAccount() {
        return toAccount;
    }

    public String getMessage() {
        return message;
    }
}
