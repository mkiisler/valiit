import java.util.Scanner;

public class Day3ForEx {

    public static void main(String[] args) {

        /*Ülesanne 11:
● Kirjuta while-tsükkel, mis prindiks standardväljundisse
numbrid 1 ... 100 - iga number eraldi real.*/


//      int numberToPrint = 1;
//      while (numberToPrint <=100){
//          System.out.println(numberToPrint++);
//      }




       /* for (int i = 0; i < 100; i++) {
            System.out.println(i);

            boolean notEnoughtMoney = true;
            int sum = 0;

            while (notEnoughtMoney) {

                sum = sum + 50; // kutsume välja mingi funktsiooni, mis tagastab meile lisanduva rahahulga ???
                if (sum >= 5000) {
                    notEnoughtMoney = false;
                }

            }
            System.out.println("Teenitav raha: " + sum);*/



/*Ülesanne 12:
● Kirjuta for-tsükkel, mis prindiks standardväljundisse numbrid
1 ... 100 - iga number eraldi real.*/


//        for (int i = 1; i < 101; i++) {
//            System.out.println(i);


/*
Ülesanne 13:
● Kirjuta foreach-tsükkel, mis prindiks standardväljundisse
numbrid 1 ... 10 - iga number eraldi real.
*/

//            int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
//            for (int num : numbers) {
//                System.out.println(num);
//    }


/*Ülesanne 14:
● Kirjuta for-tsükkel, mis prindiks standardväljundisse kõik
arvud vahemikus 1 ... 100, mis jaguvad 3-ga - iga number eraldi
real.*/
// Vale lahendus, ei läbi 1-te
//
//            for (int j = 3; j < 101; j += 3) {
//                System.out.println(j);

        // Vale lahendus, liiga keeruline
//            for (int j = 1; (j * 3) <= 100; j++) {
//                System.out.println(j * 3);

        // Õige lahendus

//            for (int o = 1; o <= 100; o++) {
//                if (o % 3 == 0) {
//                    System.out.println(o);
//                }
//
//
//            }
//        }

        /*Ülesanne 15:
● Defineeri massiiv väärtustega “Sun”, “Metsatöll”, “Queen”,
“Meallica”.
● Prindi selle massiivi väärtused for-tsükli abiga
        standardväljundisse, eraldades nad komaga. Viimase elemendi
        järele koma panna ei tohi. Tulemus peaks olema: “Sun,
                Metsatöll, Queen, Metallica”.*/

//        String[] bands = {"Sun", "Metsatöll", "Queen", "Meallica"};
//        String commaSeparatedValues = "";
//
//        for (int i = 0; i < bands.length; i++) {
//            commaSeparatedValues = commaSeparatedValues + bands[i];
//            if (i + 1 < bands.length) {
//                commaSeparatedValues = commaSeparatedValues + ", ";
//            }
//        }
//        System.out.println(commaSeparatedValues);


        /*Ülesanne 16:
● Tee sama, mis eelmises ülesandes, ainult et prindi bändid
        välja tagant poolt ettepoole.
*/
//        String[] bands2 = {"Sun", "Metsatöll", "Queen", "Meallica"};
//        String commaSeparatedValues2 = "";
//
//        for (int j = bands.length - 1; j >= 0; j--) {
//            commaSeparatedValues2 = commaSeparatedValues2 + bands[j];
//            if (j > 0) {
//                commaSeparatedValues2 = commaSeparatedValues2 + ", ";
//            }
//        }
//        System.out.println(commaSeparatedValues2);

        /*Ülesanne 17:
● Arenda Java programm, mis loeb käsurealt sisendina sisse
        numbrid vahemikus 0 - 9 ja prindib seejärel ekraanile
        sisestatud numbrid tekstilisel kujul.
        Näiteks: sisendparameetrid 4 5 6, väljaprint: neli, viis,
                kuus.
                Vihje: switch, while, for*/

//        String[] numberWords = {"null", "üks", "kaks", "kolm", "neli", "viis", "kuus", "seitse", "kaheksa", "üheksa"};
//
//        if (args.length > 0) {
//            int index = 0;
//            for (String numberText : args){
//                int numberValue = Integer.parseInt(numberText);
//                System.out.print(numberWords[numberValue]); // ei saa aru sellest reast !!! ???
//                if (index < args.length - 1) {
//                    System.out.print(", ");
//                }
//                index++;
//
//            }
//
//        }

        //Scanneriga

        Scanner arvuLugeja = new Scanner(System.in);
        System.out.println("Palun sisesta arv vahemikus 0 - 9!\n");

        int inputValue = arvuLugeja.nextInt();

        String arvSonaga;


        switch (inputValue) {
            case 0:
                arvSonaga = "null";
                break;
            case 1:
                arvSonaga = "üks";
                break;
            case 2:
                arvSonaga = "kaks";
                break;
            case 3:
                arvSonaga = "kolm";
                break;
            case 4:
                arvSonaga = "neli";
                break;
            case 5:
                arvSonaga = "viis";
                break;
            case 6:
                arvSonaga = "kuus";
                break;
            case 7:
                arvSonaga = "seitse";
                break;
            case 8:
                arvSonaga = "kaheksa";
                break;
            case 9:
                arvSonaga = "üheksa";
                break;
            default:
                arvSonaga = "See arv pole vahemikus 0 - 9!";
        }
        System.out.println(arvSonaga);
    }

}





