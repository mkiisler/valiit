import java.util.*;

public class Day4CollectionsEx {

    public static void main(String[] args) {

/*Ülesanne 19: ArrayList
● Defineeri ArrayList-tüüpi kollektsioon, mis hoiaks endas
String-tüüpi elemente.
● Sisesta kollektsooni viis suvalist linnanime.
● Prindi standardväljundisse esimene, kolmas ja viimane element
sellest kollektsioonist.*/

        List<String> linnad = new ArrayList<>();

        linnad.add("Tartu");
        linnad.add("Tõrva");
        linnad.add("Elva");
        linnad.add("Narva");
        linnad.add("Riia");

        System.out.println(linnad.get(0) + ", " + linnad.get(2) + ", " + linnad.get(4));



/*Ülesanne 20:
● Defineeri Queue-tüüpi kollektsioon (LinkedList), mis hoiaks
endas String-tüüpi elemente.
● Sisesta kollektsiooni kuus suvalist nime.
● Kasutades while-konstruktsiooni, prindi kõik nimed ükshaaval
standardväljundisse.*/


        Queue<String> names = new LinkedList<>(); // first in first out
        names.add("Mary");
        names.add("Anu");
        names.add("Ave");
        names.add("Liis");
        names.add("Lauri");
        names.add("Mati");

        //   names.peek()  // ütleb järgmise elemendi nime
        //      names.remove() // eemaldab järgmise
        while (!names.isEmpty()) {
            System.out.println("Tõmbasin sellise nime: " + names.remove());
        }


/*Ülesanne 21
            ● Defineeri Set-tüüpi kollektsioon (TreeSet), mis hoiaks
        String-tüüpi elemente.
● Prindi kollektsiooni sisu standardväljundisse, kasutades
        selle kollektsiooni forEach()-meetodit.*/

// ForEach- meetodit saab kasutada ArrayListi, Seti ja Mapiga

        Set<String> nameSet = new TreeSet<>();
        nameSet.add("Mary");
        nameSet.add("Anu");
        nameSet.add("Ave");
        nameSet.add("Liis");
        nameSet.add("Lauri");
        nameSet.add("Mati");

        nameSet.forEach(myNameToPrint -> System.out.println(("\nLambda avaldis prindib: " + myNameToPrint))); // Lambda avaldis

        // nameSet.forEach(System.out.println);

        System.out.println(nameSet);


        /*Ülesanne 22:
● Defineeri Map-tüüpi objekt (HashMap), mille key-element oleks
        String tüüpi, ja value-element oleks Stringide massiiv.
● Sisesta sellesse objekti järgmine struktuur:
○ Estonia -> Tallinn, Tartu, Valga, Võru
○ Sweden -> Stockholm, Uppsala, Lund, Köping
○ Finland -> Helsinki, Espoo, Hanko, Jämsä
● Prindi see struktuur välja, kasutades for-tsükleid (kaks
                tükki).
● Prindi see struktuur välja, kasutades map-objekti
        forEach()-meetodit.*/

        Map<String, String[]> countryCities = new HashMap<>();

        countryCities.put("Estonia", new String[]{"Tallinn", "Tartu", "Valga", "Võru"});
        countryCities.put("Sweden", new String[]{"Stockholm", "Uppsala", "Lund", "Köping"});
        countryCities.put("Finland", new String[]{"Helsinki", "Espoo", "Hanko", "Jämsä"});

        for (String country : countryCities.keySet()) {
            System.out.println("Country: " + country);
            String[] currentCountryCities = countryCities.get(country);
            System.out.println("Cities: ");
            for (String city : currentCountryCities){
                System.out.println("\t" + city);
            }
        }

    }
}
