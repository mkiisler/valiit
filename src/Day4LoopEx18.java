public class Day4LoopEx18 {
    public static void main(String[] args) {

       /* Ülesanne 18: do-while
● Kirjuta do-while tsükkel, mis prindib välja “tere” vähemalt
        üks kord.
● Defineeri double-tüüpi muutuja, milles hoida juhuslikku
        väärtust vahemikus 0 - 1.
● Pärast igat “tere” väljatrükki standardväljundisse genereeri
        uus juhuslik number vahemikus 0 - 1 ja salvesta see
        eelpooldefineeritud muutujasse.
● Jätka “tere” väljaprintimist niikaua, kui juhuslikult
        genereeritud numbri väärtus on väiksem, kui 0.5.
● Vihje: Math.random()*/

        double randomNumber;
        do {
            System.out.println("tere");
            randomNumber = Math.random();


        } while (randomNumber < 0.5);


    }

}
