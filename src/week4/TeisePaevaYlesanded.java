package week4;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TeisePaevaYlesanded {

    public static void main(String[] args) {

        // Ül 2

        System.out.println(isEqual(5, 5));

        // Ül 4

        System.out.println(giveCentury(0));
        System.out.println(giveCentury(1));
        System.out.println(giveCentury(128));
        System.out.println(giveCentury(598));
        System.out.println(giveCentury(1624));
        System.out.println(giveCentury(1827));
        System.out.println(giveCentury(1996));
        System.out.println(giveCentury(2017));
        System.out.println(giveCentury(2020));


        // Ül 5

        Country country1 = new Country();
        country1.setName("Eesti");
        country1.setPopulation(1_324_820);
        country1.setLanguages(Arrays.asList("eesti", "vene"));
        Country country2 = new Country("Läti", 1_929_900, Arrays.asList("läti", "vene"));
        System.out.println(country1.toString());
        System.out.println(country2.toString());

        System.out.println(); //vaherida

        System.out.println(makeMap("kolm", "kaheksa"));

        System.out.println(); //vaherida

        System.out.println(makeMap2("seitse", "seitseteist"));

    }

    /*Ülesanne 2:
    Kirjuta meetod, mis tagastab boolean-tüüpi väärtuse ja mille sisendparameetriteks on kaks täisarvulist muutujat.
    Meetod tagastab tõeväärtuse vastavalt sellele, kas kaks sisendparametrit on omavahel võrdsed või mitte. Meetodi nime võid ise välja mõelda.
    Kutsu see meetod main()-meetodist välja ja prindi tulemus standardväljundisse.*/

    public static boolean isEqual(int a, int b) {
//        if (a == b) {
//            return true;
//        }
//        return false;

        return a == b;
    }



   /* Ülesanne 4:
Kirjuta meetod, mis võtab sisendparameetriks aastaarvu täisarvulisel kujul (int) vahemikus 1 - 2018 ja tagastab täisarvu
(byte) vahemikus 1 - 21 vastavalt sellele, mitmendasse sajandisse antud aasta kuulub. Meetodi nime võid ise välja mõelda.
Kui funktsioonile antakse ette parameeter, mis on kas suurem, kui 2018 või väiksem, kui 1, tuleb tagastada väärtus -1;

Kutsu see meetod välja main()-meetodist järgmiste erinevate sisendväärtustega: 0, 1, 128, 598, 1624, 1827, 1996, 2017.
Prindi need väärtused standardväljundisse.
*/

    public static byte giveCentury(int year) {
        if (year < 1 || year > 2019) {
            return -1;
        } else {
            int century = year / 100;
            return (byte) (century + 1);
        }


    }

    // Tee funktsioon, mis võtab sisendiks 2 stringi ja tagastab mapi.
    //  sisend1 --> [sisend1 stringi pikkus]

    public static Map<String, String> makeMap(String sisend1, String sisend2) {

        Map<String, String> prooviMap = new HashMap<>();
        prooviMap.put(sisend1, String.valueOf(sisend1.length()));
        prooviMap.put(sisend2, String.valueOf(sisend2.length()));

        return prooviMap;
    }


    public static Map<String, Integer> makeMap2(String sisend1, String sisend2) {

        Map<String, Integer> prooviMap = new HashMap<>();
        prooviMap.put(sisend1, (Integer)sisend1.length());
        prooviMap.put(sisend2, (Integer)sisend2.length());

        return prooviMap;
    }

/*
Ülesanne 5:
Defineeri klass Country, millel oleksid meetodid getPopulation, setPopulation, getName, setName ja list riigis enim kõneldavate keeltega.
Override’i selle klassi toString() meetod nii, et selle meetodi väljakutsumine tagastaks teksti, mis sisaldaks endas kõiki parameetreid väljaprindituna.
Tekita antud klassist üks objekt ühe vabalt valitud riigi andmetega ja prindi selle riigi info välja .toString() meetodi abil.
*/

    public static class Country {

        private String name;
        private long population;
        private List<String> languages;

        public Country(String name, long population, List<String> languages) {
            this.name = name;
            this.population = population;
            this.languages = languages;
        }

        public Country() {
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public long getPopulation() {
            return population;
        }

        public void setPopulation(long population) {
            this.population = population;
        }

        public List<String> getLanguages() {
            return languages;
        }

        public void setLanguages(List<String> languages) {
            this.languages = languages;
        }

        @Override
        public String toString() {
            return "Riigi nimi: " + getName() + "\nRahvaarv: " + getPopulation() + "\nKõneldavad keeled: " + getLanguages() + "\n";

        }

    }


}


