package week4;

public class Ylesanded {

    public static final double VAT_RATE = 1.2D;

    public static void main(String[] args) {

        System.out.println(deriveNetPrice(4));
        System.out.println(isPalindrom("Aias sadas saia"));
        System.out.println(isPalindrom2("siin"));

        // Ül 3

        System.out.println(calculateBodyMassIndex(75,180));

    }

   /* Ülesanne 1
    Kirjuta Java funktsioon, mis võtab sisendparameetriks toote hinna koos käibemaksuga ning
    tagastab toote hinna ilma käibemaksuta.
    */

    public static double deriveNetPrice(double grossPrice) {
        if (grossPrice >= 0) {
            double result = grossPrice / VAT_RATE;
            result = Math.round(result * 100.0) / 100.0;
            return result;
        }

        return 0;
    }

    /*Ülesanne 2
    Palindroom on väljend, mida võib lugeda paremalt vasakule ja vasakult paremale ning mõte
    jääb samaks (näiteks: “aias sadas saia”). Kirjuta Java funktsioon, mis tuvastaks, kas
    etteantud tekst on palindroom või mitte ning tagastaks sellele vastava tõeväärtuse.
*/
    // Ül 2 variant 1

    public static boolean isPalindrom(String text) {
        String reversedText = "";
        for (int i = 0; i < text.length(); i++) {
            reversedText = text.charAt(i) + reversedText;
        }
        return text.equalsIgnoreCase(reversedText);
    }

    // Ül 2 variant 2
    public static boolean isPalindrom2(String text2) {
        String reversedText2 = "";
        StringBuilder builder = new StringBuilder(text2);
        reversedText2 = builder.reverse().toString();
        return text2.equalsIgnoreCase(reversedText2);


    }

    /*Ülesanne 3
    Kirjuta Java fuktsioon, mis arvutaks inimese kehamassiindeksi. Sisendparameetritena võtab
    funktsioon inimese kehakaalu kilogrammides ja pikkuse sentimeetrites. Kehamassiindeks
    tuleb tagastada float tüüpi muutujana.
    Kehamassiindeksi arvutamine: inimese kehakaal jagada inimese pikkuse ruuduga. Kehakaal
    kilogrammides, pikkus meetrites.
    Näiteks: inimesel, kelle pikkus on 180 cm ja kaal 75 kg, on kehamassiindeks 23.15.*/

    public static float calculateBodyMassIndex (double weightInKg, double heightInCm) {
        double heightInM = heightInCm/100;
        double bodyMassIndex = weightInKg/(heightInM * heightInM);

        float bodyMassIndexF = (float)bodyMassIndex;
        bodyMassIndexF = Math.round(bodyMassIndexF *100.0f)/100.0f;
        return bodyMassIndexF;
    }





}
