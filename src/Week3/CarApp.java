package Week3;

import java.util.*;

public class CarApp {

    public static void main(String[] args) {

        Car opelAstra = new Car(1200, 165, "Opel", "Astra", "2001-06-11");
        Car fordFocus = new Car(1350, 175, "Ford", "Focus", "2003-09-24");
        Car ferrariF355 = new Car(1850, 325, "Ferrari", "F355", "1999-02-12");
        Car audi80 = new Car(1300, 180, "Audi", "80", "1992-04-26");

        List<Car> cars = Arrays.asList(opelAstra, fordFocus, ferrariF355, audi80);
        System.out.println("Autod: " + cars);

        Car opelAstra2 = new Car(1200, 165, "Opel", "Astra", "2001-06-11");
        System.out.println("Kas Opelid on sarnased: " + opelAstra.equals(opelAstra2)); // Kui equals-meetodit ümber ei kirjuta, siis //
        // siit tuleb väljatrükk "Kas Opelid on sarnased: false" . sest objektid ei ole samad, tuleb teha võrdlemise alus

        System.out.println(opelAstra.hashCode() + " " + opelAstra2.hashCode());

        // Collections.sort(cars, INSTRUKTSIOON, KUIDAS SORTEERIDA);

//        Comparator<Car> instructionToSortCars = new CarSortingInstructions();
//        @Override
//        public int compare (Car o1, Car o2){
//            return 0;
//
//        }


        Comparator<Car> instructionToSortCars = new CarSortingInstructions();
        Collections.sort(cars, instructionToSortCars);
        System.out.println(cars);

    }

}
