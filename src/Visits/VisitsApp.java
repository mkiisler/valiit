package Visits;

import java.io.IOException;

public class VisitsApp {

    public static void main(String[] args) throws IOException {

        Visits.loadVisits(args[0]);

        Visits.findTheMostVisitsDate();

        Visits.printSortedByVisitsNumber();
    }
}
