package Visits;

import Pank.Account;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Visits {

    private String date;
    private int visitsNumber;

    public Visits(String date, int visitsNumber) {
        this.date = date;
        this.visitsNumber = visitsNumber;
    }

    public String getDate() {
        return date;
    }

    public int getVisitsNumber() {
        return visitsNumber;
    }

    /*
    Kirjutada Java programm, mis loeb failist visits.txt sisse looduspargi külastajad erinevatel jaanuari päevadel
     */

    private static List<Visits> visits = new ArrayList<>();

    public static void loadVisits(String addressOfFile) throws IOException {
        List<String> visitsRows = Files.readAllLines(Paths.get(addressOfFile));

        for (String visitsStr : visitsRows) {
            String[] partsOfRows = visitsStr.split(", ");
            String date = partsOfRows[0];
            int visitsNumber = Integer.parseInt(partsOfRows[1]);
            Visits visit = new Visits(date, visitsNumber);
            visits.add(visit);

        }
    }

    /*
    a) prindib standardväljundisse päeva, mil külastajaid oli kõige rohkem
     */

    public static void findTheMostVisitsDate() {
        int i = 0;
        int mostVisitsNumber = 0;
        String mostVisitsDate = null;
        for (; i < visits.size(); i++) {
//            Visits currentVisit = visits.get(i);
            if (visits.get(i).getVisitsNumber() > mostVisitsNumber) {
                mostVisitsNumber = visits.get(i).getVisitsNumber();
                mostVisitsDate = visits.get(i).getDate();

            }

        }
        System.out.println(mostVisitsDate + " oli külastajaid " + mostVisitsNumber + ".");
    }

    /*
    b) Sorteerib külastuspäevad külastajate arvu järgi kasvavalt ning prindib tulemuse ekraanile.
     */

    public static Comparator<Visits> howToSort = new Comparator<Visits>() {
        @Override
        public int compare(Visits v1, Visits v2) {
//            return (v1.getVisitsNumber() < v2.getVisitsNumber()) ? -1 :
//            (v2.getVisitsNumber() == v1.getVisitsNumber() ? 0 : 1); // miks seda teist rida on vaja ???
//
            return v1.getVisitsNumber() - v2.getVisitsNumber();
        }
    }; // miks siin on seminkoolon ???

    public static void printSortedByVisitsNumber() {

        Collections.sort(visits, howToSort);

        for (int i = 0; i < visits.size(); i++) {

            System.out.println(visits.get(i).getDate() + ", " + visits.get(i).getVisitsNumber());


        }
    }
}



