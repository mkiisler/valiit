public class EsimeseNadalaLisaulesanded {

    public static void main(String[] args) {

/* Kasutades for-tsükleid, joonistage standardväljundisse järgmised mustrid:

Muster 1: (vihje: mooduliga jagamine)
#+#+#+#+#+#+#+#+#+#
+#+#+#+#+#+#+#+#+#+
#+#+#+#+#+#+#+#+#+#
+#+#+#+#+#+#+#+#+#+
#+#+#+#+#+#+#+#+#+#
+#+#+#+#+#+#+#+#+#+
#+#+#+#+#+#+#+#+#+#
+#+#+#+#+#+#+#+#+#+
*/
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 19; col++) {
                if ((row % 2 == 0 && col % 2 == 0) || (row % 2 == 1 && col % 2 == 1)) {
                    System.out.print("#");
                } else {
                    System.out.print("+");
                }
            }
            System.out.println();
        }

        System.out.println(); //vaherida


/* Muster 2:
#
-#
--#
---#
----#
-----#
*/
        String mark = "#";
        int i = 6;
        do {
            System.out.println(mark);
            mark = "-" + mark;
            i--;

        } while (i > 0);

        System.out.println(); //vaherida


/*
Muster 3: (vihje: mooduliga jagamine)
#++#++#
=======
#++#++#
=======
#++#++#
 */

        for (int row = 0; row < 5; row++) {
            for (int col = 0; col < 7; col++) {
                if (row % 2 == 0 && col % 3 == 0) {
                    System.out.print("#");
                } else if (row % 2 == 1) {
                    System.out.print("=");
                } else {
                    System.out.print("+");
                }
            }
            System.out.println();


        }

        System.out.println(); //vaherida

        /* 1) Kasutades for-tsükleid ja if-lauseid, kuva ekraanil järgnev muster...
###+++###+++###+++
+++###+++###+++###
###+++###+++###+++
+++###+++###+++###
###+++###+++###+++
+++###+++###+++###
###+++###+++###+++
+++###+++###+++###
###+++###+++###+++
*/

        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 18; col++) {
                if ((row % 2 == 0 && col % 6 < 3) || (row % 2 == 1 && col % 6 >= 3)) {
                    System.out.print("#");
                } else {
                    System.out.print("+");
                }
            }
            System.out.println();
        }

        System.out.println(); //vaherida


/* 2) Kasutades for-tsükleid ja if-lauseid, kuva ekraanil järgnev muster...

#+#+#+#+#+#+#+#+#+
#+#+#+#+#+#+#+#+#+
#+#+#+#+#+#+#+#+#+
+#+#+#+#+#+#+#+#+#
+#+#+#+#+#+#+#+#+#
+#+#+#+#+#+#+#+#+#
#+#+#+#+#+#+#+#+#+
#+#+#+#+#+#+#+#+#+
#+#+#+#+#+#+#+#+#+
 */

        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 18; col++) {
                if ((row % 6 < 3 && col % 2 == 0) || (row % 6 >= 3 && col % 2 == 1)) {
                    System.out.print("#");
                } else {
                    System.out.print("+");
                }
            }
            System.out.println();
        }

        System.out.println(); //vaherida

/* 3) Kasutades for-tsükleid ja if-lauseid, kuva ekraanil järgnev muster...
#
#
 #
  #
   #
    #
     #
      #
       #
        #
         #
          #
           #
            #
             #
              #
               #
                #
*/

        String mark3 = "#";
        int j = 17;
        System.out.println(mark3);
        do {
            System.out.println(mark3);
            mark3 = " " + mark3;
            j--;

        } while (j > 0);

        System.out.println(); //vaherida

        for (int row3 = 0; row3 < 18; row3++) {
            for (int col3 = 0; col3 < 18; col3++) {
                if (row3 == col3) {
                    System.out.print("#");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }

/*4) Kasutades for-tsükleid ja if-lauseid, kuva ekraanil järgnev muster...
                #
               #
              #
             #
            #
           #
          #
         #
        #
       #
      #
     #
    #
   #
  #
 #
#
#*/

        for (int row4 = 18; row4 > 0; row4--) {
            for (int col4 = 0; col4 < 18; col4++) {
                if (row4 == col4) {
                    System.out.print("#");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }

        for (int row = 0; row < 18; row++) {
            for (int col = 0; col < 18; col++) {
                if (row + col == 17) {
                    System.out.print("#");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }

    }
}
