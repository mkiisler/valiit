public class Day2Ex2 {

    public static void main(String[] args) {

        // Muutujate defineerimine
        /*Ülesanne 2: valikute konstrueerimine
● Defineeri String-tüüpi muutuja ja anna talle väärtuseks
“Berlin”.
● Konstrueeri if-lause, mis kontrolliks, kas antud muutuja
        väärtus on “Milano”. Kui on, siis kirjuta standardväljundisse
        lause: “Ilm on soe.” Kui mitte, siis kirjuta: “Ilm polegi
        kõige tähtsam!”
*/
        String linn = "Berliin";

        if (linn.equals("Milano")) { // !Sõnede võrdlemine!!
            System.out.println("Ilm on ilus.");

        } else {
            System.out.println("Ilm polegi kõige tähtsam.");
        }

        /*Ülesanne 3: if, else if, else
● Defineeri muutuja, mis hoiaks täisarvulist väärtust vahemikus
        1 - 5. Väärtusta see muutuja.
● Kasuta “if”-”else if”-”else” konstruktsiooni, et printida
        ekraanile kas “nõrk”, “mitterahuldav”, “rahuldav”, “hea”,
“väga hea” - vastavalt sellele, mis hinne eelpooldefineeritud
        muutujasse salvestatud.*/

        int hinne = 3;
        String sonalineHinne;

        if (hinne == 1) {
            sonalineHinne = "nõrk";
        } else if (hinne == 2) {
            sonalineHinne = "mitterahuldav";
        } else if (hinne == 3) {
            sonalineHinne = "rahuldav";
        } else if (hinne == 4) {
            sonalineHinne = "hea";
        } else {
            sonalineHinne = "väga hea";
        }

        System.out.println(sonalineHinne);

        /*Ülesanne 4: switch
● Defineeri muutuja, mis hoiaks täisarvulist väärtust vahemikus
1 - 5. Väärtusta see muutuja.
● Kasuta switch-konstruktsiooni, et printida ekraanile kas
“nõrk”, “mitterahuldav”, “rahuldav”, “hea”, “väga hea” -
vastavalt sellele, mis hinne eelpooldefineeritud muutujasse
salvestatud.
*/
        int hinne4 = 4;
        String sonalineHinne4;

        switch (hinne4) {
            case 1:
                sonalineHinne = "nõrk";
                break;
            case 2:
                sonalineHinne = "mitterahuldav";
                break;
            case 3:
                sonalineHinne = "rahuldav";
                break;
            case 4:
                sonalineHinne = "hea";
                break;
            default:
                sonalineHinne = "väga hea";
        }

        System.out.println(sonalineHinne);


        /*Ülesanne 5: inline-if
● Defineeri muutuja, mis hoiaks endas täisarvulist väärtust.
● Kasutades inline-if konstruktsiooni, kirjuta
        standardväljundisse kas “Noor” või “Vana”, vastavalt sellele,
        kas muutuja väärtus on suurem, kui 100 või mitte.
*/

        int aastaid = 8;

        String vanus = aastaid <= 100 ? "Noor." : "Vana.";
            System.out.println(vanus);


        /*Ülesanne 6: inline-if
● Defineeri muutuja, mis hoiaks endas täisarvulist väärtust.
● Kasutades inline-if konstruktsiooni, kirjuta
        standardväljundisse kas “Noor”, “Vana” või “Peaaegu vana”,

        vastavalt sellele, kas inimene vanus on alla saja, üle saja
        või täpselt sada.*/

        int aastaid6 = 110;

        String vanus6 = aastaid6 < 100 ? "Noor." : aastaid6 == 100 ? "Peaaegu vana." : "Vana.";
        System.out.println(vanus6);


    }
}
